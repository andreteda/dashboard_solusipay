<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<style>
	.more {
		background:white;
		border:0.1em solid #acafb3;
		cursor:pointer;
    outline: none;
    border-radius: 15px;
    width: 120px;
  }
    
  .morered {
		background:white;
		border:0.1em solid #acafb3;
		cursor:pointer;
    outline: none;
    border-radius: 15px;
    width: 120px;
	}

  .more:hover {
      border: 2px solid #4CAF50;
  }

  .more:active {
      background-color: #3e8e41;
  }

  .morered:hover {
      border: 2px solid red;
  }

  .morered:active {
      background-color: #3e8e41;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
      <h1>
        {{ $subtitle }}
        <small>{{ $sub_ }}</small>
      </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xl-12 col-12">
        <div class="box bg-teal">
          <div class="box-body">
            <div class="row">
                <input type="text" value="{{ $clientID }}" name="client" id="client" hidden/>
                @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
                <div class="col-md-3">
                    Partner Name :
                    <div class="input-group input-group-sm">
                    <select name="partnerid" id="partnerid" class="form-control">
                        <option value="ALL">-- ALL --</option>
                        @foreach ($data_partner as $item)
                        <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                @endif
                <div class="col-md-3">
                    Payment Method :
                    <div class="input-group input-group-sm">
                    <select name="partner" id="partner" class="form-control">
                        <option value="ALL">-- ALL --</option>
                        @foreach ($data_pm as $item)
                        <option value="{{ $item->pay_methode }}"> {{ $item->methode_desc  }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <br>
                    <button type="button" id="search" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
                @if(Auth::user()->roleid == 1)
                <div class="col-md-5">
                  <div class="box-tools pull-right">
                      <a href="#" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-right">Add New Payment Method</a>
                  </div>
                </div>
                @endif
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-body">
            <div class="table-responsive">
              <table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                  <tr>
                    <th class="text-center">Payment Method</th>
                    <th class="text-center">Description</th>
                    <th class="text-center">IsAccess</th>
                    <th class="text-center">Status</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<div class="modal modal-right fade" id="modal-right" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add New Payment Method</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<h5>Payment Method Name</h5>
						<div class="controls">
							<input type="text" id="name" class="form-control" required data-validation-required-message="This field is required">
						</div>
          </div>
          <div class="form-group">
						<h5>Payment Method Name</h5>
						<div class="controls">
							<textarea rows="4" cols="50" id="desc" class="form-control" required data-validation-required-message="This field is required"> </textarea>
						</div>
					</div>
					<div class="form-group">
						<h5>Status</h5>
						<div class="controls">
							<select name="select" id="status" required class="form-control">
                <option value="0">NON-ACTIVE</option>
								<option value="1">ACTIVE</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="add_pm" class="btn btn-bold btn-pure btn-primary float-right">SAVE NEW PAYMENT METHOD</button>
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/pages/data-table.js') }}"></script>
<script>
  $(function () {
    "use strict";

    $.xhrPool = [];
    $.xhrPool.abortAll = function() {
      $(this).each(function(idx, jqXHR) {
          jqXHR.abort();
      });
      $.xhrPool = [];
    };

    $.ajaxSetup({
      beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
      },
      complete: function(jqXHR) {
        var index = $.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });

	$(document).ready(function () {
		$table = $('#datalist').DataTable({
      "processing": true,
      "serverSide": true,
      "dom": "<'top'ip>rt",
      "pageLength" : 10,
      "ordering": false,
      "ajax": {
        "url": "{{ route('data-pm') }}",
        "type":"POST",
        "data": function(d){
            d.partnerid = document.getElementsByName('partnerid')[0].value,
            d.paymentid = document.getElementsByName('partner')[0].value,
            d._token = '{{ Session::token() }}'
        }
      },
			"columns": [
        { "data" : "pay_methode"},
        { "data" : "methode_desc"},
        { 
          "data" : "IsAccess",
          "visible" : false
        },
        { 
          mRender: function (data, type, row) {
              if (row.IsAccess == "1") {
                  return '<div style="text-align: center;"><button class="morered glyphicon glyphicon-stop" style="color:red" onclick="updateIt(\'' + row['pay_methode'] + '\',\'' + row['IsAccess'] + '\')"> <span style="font-family:Trebuchet MS">NON-ACTIVE</span></button></div>';
              }else {
                  return '<div style="text-align: center;"><button class="more glyphicon glyphicon-play" style="color:green" onclick="updateIt(\'' + row['pay_methode'] + '\',\'' + row['IsAccess'] + '\')"> <span style="font-family:Trebuchet MS">ACTIVE</span></button></div>';
              }
            }
        }
			]
		});
  });
  
  //search
  $('#search').click(function(e){
    e.preventDefault();
    cloneInput();
    $table.draw();
  });

  function cloneInput() {
    var $box = $("#box");
    var $thinput = $(".thinput").find("input");

    $box.empty();
    $thinput.clone().appendTo("#box").find("input").each(function() {
        this.attr('id','');
        this.attr('type','hidden');
    });
  }

	$('#add_pm').click(function(){
		$.post("{{ route('add-pm') }}", {
			name	  : $('#name').val(),
      desc 	  : $('#desc').val(),
      status	: $('#status').val(),
			_token	: '{{ Session::token() }}'
		},
		function(data, status){
			if(data) {
				swal("Failed!", "Add new Payment Method", "danger");
			} else {
				swal("Success!", "Add new Payment Method", "success");
				$table.draw();
			}
    });
  });
  
  function updateIt(pay_methode,status) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Update it!",
			cancelButtonText: "No, Cancel!",
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm) {
			if (isConfirm) {
        // console.log(pay_methode,status);
				$.post("{{ route('update-pm') }}", {
					pay_methode	: pay_methode,
          status		  : status,
					_token		  : '{{ Session::token() }}'
				},
				function(data, status){
					if(data) {
						swal("Failed!", "Failed Update Payment Methode.", "danger");
					} else {
						swal("Success!", "Update Payment Methode Successfully.", "success");
						$table.draw();
					}
				});
			}
			else{
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
		});
	}

</script>








