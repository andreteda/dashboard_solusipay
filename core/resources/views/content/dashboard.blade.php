<div class="content-wrapper">
  <section class="content-header">
      <h1>
        Dashboard
      </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xl-12 col-12">
      <input type="text" value="{{ Auth::user()->roleid }}" name="client" id="client" hidden/>
        @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
        <div class="box bg-teal">
          <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    Partner Name :
                    <div class="input-group input-group-sm">
                    <select name="partner" id="partner" class="form-control">
                        <option value="ALL">-- ALL --</option>
                        @foreach ($data_partner as $item)
                        <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="col-md-1">
                    <br>
                    <button type="button" id="search" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
            </div>
          </div>
        </div>
        @endif
        <div class="box bg-teal">
          <div class="box-body">
            <div class="row">
              <div class="col-xl-9"> NOTE: This Data will updated every 15 minutes. </div>
              <div class="col-xl-3"> <div class="pull-right"> <span id="date-part"> </span> <span id="time-part"> </span></div> </div>
            </div>
          </diV>
        </div>
        <div class="box">
            <div class="box-body pt-15">
              <div class="table-responsive thinput">
		            <table id="datalist"  class="table table-bordered table-striped nowrap" border="5" bordercolor="#ff0000">
		              <thead>		
										<tr>
											<th>No</th>
											<th>Biller Name</th>
											<th>Total Trx</th>
										</tr>
									</thead>
								</table>
							</div>
            </div>
        </div>
      </div>
    </div>	
  </section>
  <!-- /.content -->
</div>

<script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
	<script>
    $(function () {
        "use strict";

        $.xhrPool = [];
        $.xhrPool.abortAll = function() {
            $(this).each(function(idx, jqXHR) {
                jqXHR.abort();
            });
            $.xhrPool = [];
        };

        $.ajaxSetup({
            beforeSend: function(jqXHR) {
                $.xhrPool.push(jqXHR);
            },
            complete: function(jqXHR) {
                var index = $.xhrPool.indexOf(jqXHR);
                if (index > -1) {
                    $.xhrPool.splice(index, 1);
                }
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    //search
    $('#search').click(function(e){
      e.preventDefault();
      cloneInput();
      $table.draw();
    });

    function cloneInput() {
      var $box = $("#box");
      var $thinput = $(".thinput").find("input");

      $box.empty();
      $thinput.clone().appendTo("#box").find("input").each(function() {
          this.attr('id','');
          this.attr('type','hidden');
      });
    }

		var objTable = {};
	    $(document).ready(function () {
        $table = $('#datalist').DataTable({
          "processing": true,
          "serverSide": true,
          "dom": 'Brtip',
          "pageLength" : 100,
          "ordering": false,
          "ajax" : {
            "url" : "{{ route('load-dashboard') }}",
            "type":"POST",
            "data" : function(d){
                if(document.getElementsByName('client')[0].value == '1' || document.getElementsByName('client')[0].value == '2' ){
                  d.partnerid = document.getElementsByName('partner')[0].value,
                  d._token = '{{ Session::token() }}'
                }else{
                  d._token = '{{ Session::token() }}'
                }
              }
            },
            "columns": [
              { "data": null,"sortable": false,
                  render: function (data, type, row, meta) {
                      return meta.row + meta.settings._iDisplayStart + 1;
                  }
              },
              {
                  mRender: function (data, type, row) {
                      return '<span style="font-size:12px; font-weight: 500;">' + row['biller_name'] + '</span>';
                  }
              },
              {
                  mRender: function (data, type, row) {
                      return '<span style="font-size:12px; font-weight: 500;">' + row['jml_trx'] + '</span>';
                  }
              }
            ],
        initComplete: function () {
        }
      });

      var interval = setInterval(function() {
        var momentNow = moment();
        $('#date-part').html(momentNow.format('dddd') + ', ' + momentNow.format('DD MMM YYYY'));
        $('#time-part').html(momentNow.format('HH:mm:ss'));
      }, 100);
    });
    
    setInterval(function() {
      $table.ajax.reload();
    }, 900000 );
	</script>