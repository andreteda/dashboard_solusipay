<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">

<div class="content-wrapper">
    <section class="content-header">
      	<h1>
        	{{ $subtitle }}
        	<small>{{ $sub_ }}</small>
      	</h1>
    </section>
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xl-12 col-12">
	          	<div class="box">
		            <div class="box-header with-border">
		              	<h3 class="box-title">User Role</h3>
		              	<div class="box-tools pull-right">
			                <a href="#" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-right">Add New Role</a>
		              	</div>
		            </div>
	            	<div class="box-body">
	            		<div class="table-responsive">
	            		<table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
	            			<thead>
								<tr>
								  	<th>#</th>
								  	<th>Role Code</th>
								  	<th>Role Name</th>
								  	<th>Status</th>
								</tr>
							</thead>
	            		</table>
	            		</div>
	            	</div>
	          	</div>
	        </div>
	    </div>
	</section>
</div>
<div class="modal modal-right fade" id="modal-right" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add New User Role</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<h5>User Role Code</h5>
						<div class="controls">
							<input type="text" id="role_code" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
					<div class="form-group">
						<h5>User Role Name</h5>
						<div class="controls">
							<input type="text" id="role_name" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="add_role" class="btn btn-bold btn-pure btn-primary float-right">SAVE NEW ROLE</button>
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/pages/data-table.js') }}"></script>
<script>
	var objTable = {};
	$(document).ready(function () {
		objTable.data = $('#datalist').DataTable({
			"dom": "<'top'ip>rt",
			"ajax": {
				"url": "{{ route('data-role') }}",
				"dataSrc": ""
			},
			"columns": [
			{ "data" : "role_id"},
			{ "data" : "role_name"},
			{ "data" : "role_desc"},
			{
				mRender: function (data, type, row) {
					return '<button type="button" class="btn btn-block btn-danger btn-flat" onclick="deleteIt(' +row.role_id+ ')">DELETE</button>';
				}
			}
			]
		});
	});
	$('#add_role').click(function(){
		$.post("{{ route('add-role') }}", {
			role_code	: $('#role_code').val(),
			role_name 	: $('#role_name').val(),
			_token		: '{{ Session::token() }}'
		},
		function(data, status){
			if(data) {
				swal("Failed!", "Add new Role", "danger");
			} else {
				swal("Success!", "Add new Role", "success");
				//$('#modal-right').modal('hide');
				objTable.data.ajax.reload();
			}
        });
	});
	function deleteIt(id) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Delete it!",
			cancelButtonText: "No, Cancel!",
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm) {
			if (isConfirm) {
				$.post("{{ route('delete-role') }}", {
					role_id		: id,
					_token		: '{{ Session::token() }}'
				},
				function(data, status){
					if(data) {
						swal("Failed!", "Delete Role", "danger");
					} else {
						swal("Success!", "Delete Role", "success");
						objTable.data.ajax.reload();
					}
				});
			}
			else{
				swal("Cancelled", "Your imaginary file is safe :)", "error");
			}
		});
	}
</script>








