<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<style>
	.more {
		background:none;
		border:none;
		cursor:pointer;
	}
</style>
<div class="content-wrapper">
    <section class="content-header">
      	<h1>
        	{{ $subtitle }}
        	<small>{{ $sub_ }}</small>
      	</h1>
    </section>
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xl-12 col-12">
	          	<div class="box">
		            <div class="box-header with-border">
		              	<h3 class="box-title">List User</h3>
		              	<div class="box-tools pull-right">
			                <a href="#" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-right">Add New User</a>
		              	</div>
		            </div>
	            	<div class="box-body">
	            		<div class="table-responsive">
	            		<table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
	            			<thead>
								<tr>
								  	<th>#</th>
									<th>id</th>
								  	<th>Name</th>
								  	<th>Email</th>
								  	<th>Created At</th>
								  	<th>Role User</th>
								  	<th>Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
	            		</table>
	            		</div>
	            	</div>
	          	</div>
	        </div>
	    </div>
	</section>
</div>
<div class="modal modal-right fade" id="modal-right" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add New User Role</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<h5>Name User</h5>
						<div class="controls">
							<input type="text" id="name" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
					<div class="form-group">
						<h5>Email</h5>
						<div class="controls">
							<input type="email" id="email" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
					<div class="form-group">
						<h5>Password</h5>
						<div class="controls">
							<input type="password" id="password" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
					<div class="form-group">
						<h5>Role User</h5>
						<div class="controls">
							<select name="select" id="role" required class="form-control">
								<option value="NULL">Select Role</option>
								@foreach($role as $value)
								<option value="{{ $value->role_id }}">{{ $value->role_desc }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<h5>Status</h5>
						<div class="controls">
							<select name="select" id="status" required class="form-control">
								<option value="NULL">Select Status</option>
								<option value="1">ACTIVE</option>
								<option value="0">NON-ACTIVE</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="add_role" class="btn btn-bold btn-pure btn-primary float-right">SAVE NEW USER</button>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-right fade" id="modal-right-edit" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit User</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<h5>Name User</h5>
						<input type="hidden" class="form-control pull-right" name="id" id="id">
						<div class="controls">
							<input type="text" name="usernameedit" id="usernameedit" class="form-control" disabled>
						</div>
					</div>
					<div class="form-group">
						<h5>Mobile Phone</h5>
						<div class="controls">
							<input type="text" name="phoneedit" id="phoneedit" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<h5>Role User</h5>
						<div class="controls">
							<select name="roleedit" id="roleedit" required class="form-control">
								<option value="NULL">Select Role</option>
								@foreach($role as $value)
								<option value="{{ $value->role_id }}">{{ $value->role_desc }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<h5>Status</h5>
						<div class="controls">
							<select name="statusedit" id="statusedit" required class="form-control">
								<option value="NULL">Select Status</option>
								<option value="1">ACTIVE</option>
								<option value="0">NON-ACTIVE</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="edit_role" class="btn btn-bold btn-pure btn-primary float-right">EDIT NEW USER</button>
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('assets/vendor_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/pages/data-table.js') }}"></script>
<script>

	var objTable = {};
	$(document).ready(function () {
		objTable.data = $('#datalist').DataTable({
			"dom": "<'top'ip>rt",
			"ajax": {
				"url": "{{ route('data-user') }}",
				"dataSrc": ""
			},
			"columns": [
				{ 
					mRender: function ( data, type, row, meta ) {
						return  meta.row + 1;
					} 
				},
				{ 
					"data" : "id",
					"visible" : false
				},
				{ "data" : "username"},
				{ "data" : "email"},
				{ "data" : "createdon"},
				{ "data" : "role_desc"},
				{ 
					mRender: function (data, type, row) {
						if (row.isactive == "1") {
								return '<div style="text-align: center;"><span style="font-size:12px; color:green; font-weight: 500;">' + 'ACTIVE' + '</span></div>';
						}else {
							return '<div style="text-align: center;"><span style="font-size:12px; color:red; font-weight: 500;">' + 'NON-ACTIVE' + '</span></div>';
						}
					}
				},
				{
					mRender: function (data, type, row) {
						return '<div style="text-align: center;"><button type="button" class="more glyphicon glyphicon-pencil" style="color:green" onclick="editIt(' +row.id+ ')"> UPDATE</button><button class="more glyphicon glyphicon-remove" style="color:red" onclick="deleteIt(' +row.id+ ')"> DELETE</button></div>';
					}
				}
			]
		});
	});
	$('#add_role').click(function(){
		$.post("{{ route('add-user') }}", {
			name	: $('#name').val(),
			email 	: $('#email').val(),
			password: $('#password').val(),
			role 	: $('#role').val(),
			status 	: $('#status').val(),
			tag 	: 'INSERT',
			_token	: '{{ Session::token() }}'
		},
		function(data, status){
			if(data === "Your email already taken.") {
				swal("Failed!", data, "error");
			}else if(data){
				swal("Failed!", "Add New User", "error");
			} else {
				swal("Success!", "Add New User", "success");
				// $('#modal-right').modal('hide');
				objTable.data.ajax.reload();
			}
        });
	});
	function deleteIt(id) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Delete it!",
			cancelButtonText: "No, Cancel!",
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm) {
			if (isConfirm) {
				$.post("{{ route('delete-user') }}", {
					role_id		: id,
					_token		: '{{ Session::token() }}'
				},
				function(data, status){
					if(data) {
						swal("Failed!", "Failed Delete User.", "danger");
					} else {
						swal("Success!", "Delete User Successfully.", "success");
						objTable.data.ajax.reload();
					}
				});
			}
			else{
                swal("Cancelled", "Your imaginary file is safe :)", "error");
            }
		});
	}
	$('#edit_role').click(function(){
		var fd = new FormData();
		fd.append( "id", $('#id').val());
		fd.append( "phone", $('#phoneedit').val());
		fd.append( "role", $('#roleedit').val());
		fd.append( "status", $('#statusedit').val());
		fd.append( "tag", "UPDATE");
		fd.append( "_token", "{{ Session::token() }}");
		$.ajax({
            url: "{{ route('add-user') }}",
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data, status){
				console.log(data);
            	var datanotif = JSON.parse(data);
				if(datanotif.status == '200') {
					swal("Success!", datanotif.messages, "success");
					objTable.data.ajax.reload();
				} else {
					swal("Failed!", datanotif.messages, "danger");
				}
            },
            error: function(err){
            	swal("Error!", 'Contact Admin Solusipay to fix this error!', "danger");
            }
        });
	});
	function editIt(id) {
		$('#modal-right-edit').modal('show');
		$.post("{{route('detail-user')}}", {
            id : id,
            _token : '{{ Session::token() }}'
		},
		function(data, status){
			$('#id').val(data.id);
			$('#usernameedit').val(data.username);
			$('#phoneedit').val(data.phone);
			$('#roleedit').val(data.role);
			$('#statusedit').val(data.status);
		});
	}
</script>








