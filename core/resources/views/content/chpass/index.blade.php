<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<div class="content-wrapper">
    <section class="content-header">
      	<h1>
        	{{ $subtitle }}
        	<small>{{ $sub_ }}</small>
      	</h1>
    </section>
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xl-12 col-12">
	          	<div class="box">
		            <div class="box-header with-border">
		              	<h3 class="box-title">Welcome, {{ $username }}</h3>
		            </div>
	            	<div class="box-body">
                        <form class="form-horizontal" role="form">
                            @csrf
                            <table class="" width="50%">
                                <tr>
                                    <td colspan="6"> 
                                        <div class="form-group">
                                            <label for="txtcurrent">Current Password</label>
                                            <input type="password" class="form-control" id="oldpass" name="oldpass" rows="3" required/>
                                        </div>
                                    </td>
                                </tr>   
                                <tr>
                                    <td colspan="6"> 
                                        <div class="form-group">
                                            <label for="txtnew">New Password</label>
                                            <input type="password" class="form-control" id="newpass" name="newpass" rows="3" required/>
                                        </div>
                                    </td>
                                </tr>   
                                <tr>
                                    <td colspan="6"> 
                                        <div class="form-group">
                                            <label for="txtrepeat">Repeat Password</label>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" rows="3" required/>
                                        </div>
                                    </td>
                                </tr>   
                                <tr>
                                    <td colspan="6"> 
                                        <div class="form-group">
                                            <input type="button" class="btn btn-success btn-lg btn-block" id="btnSubmit" value="Submit" onclick="return Validate('{{ Auth::user()->id }}')" />
                                        </div>
                                    </td>
                                </tr>               
                            </table>
                        </form>
	            	</div>
	          	</div>
	        </div>
	    </div>
	</section>
</div>
<script>
    function Validate(id) {
        var oldpass = document.getElementById("oldpass").value;
        var newpass = document.getElementById("newpass").value;
        var confirmPassword = document.getElementById("password_confirmation").value;
        // console.log(newpass);
        // console.log(confirmPassword);
        if (oldpass == "" || newpass == "" || confirmPassword == ""){
            swal("Failed!", "Every field is required!", "error");
            return false;
        }
        if (newpass != confirmPassword) {
            swal("Failed!", "Confirmation Password do not match!", "error");
            return false;
        }

        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, Update it!",
            cancelButtonText: "No, Cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
        if (isConfirm) {
            $.post("{{ route('update-chpass') }}", {
			role_id		: id,
            newpass     : newpass,
            oldpass     : oldpass,
			_token		: '{{ Session::token() }}'
            },
            function(data, status){
                console.log(data);
                if(data) {
                    swal("Failed!", "Your current password is wrong!", "error");
                } else {
                    swal("Success!", "Change password is successfully!", "success");
                }
            });
            return true;
        } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
        });
    }
</script>

