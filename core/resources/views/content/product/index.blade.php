<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<style>
	.more {
		background:white;
		border:none;
		cursor:pointer;
    outline: none;
    border: none;
    border-radius: 15px;
  }
    
  .morered {
		background:white;
		border:none;
		cursor:pointer;
    outline: none;
    border: none;
    border-radius: 15px;
  }
  
  .more:hover {
      border: 2px solid #4CAF50;
  }

  .more:active {
      background-color: #3e8e41;
  }

  .morered:hover {
      border: 2px solid red;
  }

  .morered:active {
      background-color: #3e8e41;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
      <h1>
        {{ $subtitle }}
        <small>{{ $sub_ }}</small>
      </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xl-12 col-12">
        <div class="box bg-teal">
          <div class="box-body">
            <div class="row">
              <input type="text" value="{{ Auth::user()->roleid }}" name="client" id="client" hidden/>
                <div class="col-md-2">
                    Start Date:
                    <div class="input-group  input-group-sm date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="start_date" name="start_date"
                            value="{{ date('d-m-Y',strtotime('-1 day')) }}">
                    </div>
                </div>
                <div class="col-md-2">
                    End Date:
                    <div class="input-group input-group-sm date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right" id="end_date" name="end_date" value="{{ date("d-m-Y") }}">
                    </div>
                </div>
                @if(Auth::user()->roleid == 1)
                <div class="col-md-8">
                    <div class="box-tools pull-right">
                        <a href="#" type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-right">Add New Product</a>
                    </div>
                </div>
                @endif
            </div>
            <br>
            <div class="row">
              <div class="col-md-2">
                  Biller Type:
                  <div class="input-group input-group-sm">
                  <select name="biller" id="biller" class="form-control" onchange="getval(this);">
                    <option value="ALL">-- ALL --</option>
                    @foreach ($data_biller as $item)
                      <option value="{{ $item->biller_id }}"> {{ $item->biller_name  }}</option>
                    @endforeach
                  </select>
                  </div>
              </div>
              <div class="col-md-4">
                  Product Name:
                  <div class="input-group input-group-sm">
                  <select name="product" id="product" class="form-control">
                      <option value="ALL">-- ALL --</option>
                      @foreach ($data_product as $item)
                      <option value="{{ $item->productid }}"> {{ $item->Productdesc  }}</option>
                      @endforeach
                  </select>
                  </div>
              </div>
              @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
              <div class="col-md-2">
                  Partner ID :
                  <div class="input-group input-group-sm">
                  <select name="partner" id="partner" class="form-control">
                      <option value="ALL">-- ALL --</option>
                      @foreach ($data_partner as $item)
                      <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                      @endforeach
                  </select>
                  </div>
              </div>
              @endif
              <div class="col-md-1">
                  <br>
                  <button type="button" id="search" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-search"></i> Search</button>
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-body">
            <div class="table-responsive">
              <table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                  <tr>
                    <th> Biller ID</th>
                    <th class="text-center">Biller Type</th>
                    <th> Product ID</th>
                    <th class="text-center">Product Name</th>
                    <th> Partner ID</th>
                    <th class="text-center">Partner</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Price</th>
                    <th class="text-center">Created On</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<div class="modal modal-right fade" id="modal-right" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add New Product</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="form-group">
						<h5>Biller Type</h5>
						<div class="controls">
              <select name="billerAdd" id="billerAdd" class="form-control" onchange="getvalAdd(this);">
                <option value="">-- Select One --</option>
                @foreach ($data_biller as $item)
                  <option value="{{ $item->biller_id }}"> {{ $item->biller_name  }}</option>
                @endforeach
              </select>
            </div>
					</div>
					<div class="form-group">
						<h5>Product Name</h5>
						<div class="controls">
              <select name="productAdd" id="productAdd" class="form-control">
                  <option value="">-- Select One --</option>
                  @foreach ($data_product as $item)
                  <option value="{{ $item->productid }}"> {{ $item->Productdesc  }}</option>
                  @endforeach
              </select>
            </div>
					</div>
					<div class="form-group">
						<h5>Partner</h5>
            <div class="controls">
              <select name="partnerAdd" id="partnerAdd" class="form-control">
                  <option value="ALL">-- Select One --</option>
                  @foreach ($data_partner as $item)
                  <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                  @endforeach
              </select>
            </div>
					</div>
					<div class="form-group">
						<h5>Status</h5>
						<div class="controls">
							<select name="statusAdd" id="statusAdd" required class="form-control">
								<option value="1">ACTIVE</option>
								<option value="0">NON-ACTIVE</option>
							</select>
						</div>
					</div>
          <div class="form-group">
						<h5>Price</h5>
						<div class="controls">
							<input type="number" name="priceAdd" id="priceAdd" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="add_product" class="btn btn-bold btn-pure btn-primary float-right">SAVE NEW PRODUCT</button>
			</div>
		</div>
	</div>
</div>
<div class="modal modal-right fade" id="modal-right-edit" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Product</h5>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
        <h3><span id="billernameedit"> </span> - <span id="productnameedit"> </span> - <span id="partnernameedit"> </span></h3>
				<form>
          <input type="hidden" class="form-control pull-right" name="billeridedit" id="billeridedit">
          <input type="hidden" class="form-control pull-right" name="productidedit" id="productidedit">
          <input type="hidden" class="form-control pull-right" name="partneridedit" id="partneridedit">
          @if(Auth::user()->roleid == 1 || Auth::user()->roleid <> 2) 
          <div class="form-group">
						<h5>Price</h5>
						<div class="controls">
							<input type="number" name="priceedit" id="priceedit" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
          @endif
          @if(Auth::user()->roleid == 1)
					<div class="form-group">
						<h5>Nominal Promo</h5>
						<div class="controls">
							<input type="number" name="npromoEdit" id="npromoEdit" class="form-control" required data-validation-required-message="This field is required">
						</div>
					</div>
					<div class="form-group">
						<h5>Status Promo</h5>
						<div class="controls">
              <select name="statuspromoedit" id="statuspromoedit" required class="form-control">
                <option value="0">NON-ACTIVE</option>
                <option value="1">ACTIVE</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<h5>Status</h5>
						<div class="controls">
							<select name="statusedit" id="statusedit" required class="form-control">
								<option value="1">ACTIVE</option>
								<option value="0">NON-ACTIVE</option>
							</select>
						</div>
					</div>
          @endif
          <input type="number" name="npromoEdit" id="npromoEdit" class="form-control" required data-validation-required-message="This field is required" hidden>
          <select name="statuspromoedit" id="statuspromoedit" required class="form-control" hidden>
            <option value="0">NON-ACTIVE</option>
            <option value="1">ACTIVE</option>
          </select>
          <select name="statusedit" id="statusedit" required class="form-control" hidden>
            <option value="1">ACTIVE</option>
            <option value="0">NON-ACTIVE</option>
          </select>
			</div>
			<div class="modal-footer modal-footer-uniform">
				<button type="button" id="edit_product" class="btn btn-bold btn-pure btn-primary float-right">EDIT NEW PRODUCT</button>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
<script>
  $(function () {
    "use strict";

    $.xhrPool = [];
    $.xhrPool.abortAll = function() {
      $(this).each(function(idx, jqXHR) {
          jqXHR.abort();
      });
      $.xhrPool = [];
    };

    $.ajaxSetup({
      beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
      },
      complete: function(jqXHR) {
        var index = $.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });

  $(document).ready(function () {
    $table = $('#datalist').DataTable({
      "processing": true,
      "serverSide": true,
      "dom": "<'top'ip>rt",
      "pageLength" : 10,
      "ordering": false,
      "ajax": {
        "url": "{{ route('data-product') }}",
        "type":"POST",
        "data": function(d){
          if(document.getElementsByName('client')[0].value == '1' || document.getElementsByName('client')[0].value == '2' ){
            d.partnerid = document.getElementsByName('partner')[0].value,
            d.biller = document.getElementsByName('biller')[0].value,
            d.product = document.getElementsByName('product')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }else{
            d.biller = document.getElementsByName('biller')[0].value,
            d.product = document.getElementsByName('product')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }
        }
      },
      "columns": [
        { 
					"data" : "biller_id",
					"visible" : false
				},
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.biller_name+ '</div>';
          }
        },
        { 
					"data" : "product_id",
					"visible" : false
				},
        {  mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.ProductDesc+ '</div>';
          }
        },
        { 
					"data" : "partner_id",
					"visible" : false
				},
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.description+ '</div>';
          }
        },
        {
          mRender: function (data, type, row) {
						if (row.isactive == "1") {
									return '<div style="text-align: center;"><span style="font-size:12px; color:green; font-weight: 500;">' + 'ACTIVE' + '</span></div>';
						}else {
							return '<div style="text-align: center;"><span style="font-size:12px; color:red; font-weight: 500;">' + 'NON-ACTIVE' + '</span></div>';
						}
					}
        },
        { 
          mRender: function (data, type, row) {
            return '<div style="text-align: right;"> ' +row.nominal+ '</div>';
          }
        },
        { "data" : "createdon"},
        {
          mRender: function (data, type, row) {
            if(document.getElementsByName('client')[0].value == '1' || document.getElementsByName('client')[0].value == '2' ){
              return '<div style="text-align: center;"><button class="more glyphicon glyphicon-pencil" style="color:green" onclick="editIt(' +row.biller_id+ ',\'' + row['product_id'] + '\',\'' + row['partner_id'] + '\')"></button><button class="morered glyphicon glyphicon-remove" style="color:red" onclick="deleteIt(' +row.biller_id+ ',\'' + row['product_id'] + '\',\'' + row['partner_id'] + '\')"></button></div>';
            }else{
              return '<div style="text-align: center;"><button class="more glyphicon glyphicon-pencil" style="color:green" onclick="editIt(' +row.biller_id+ ',\'' + row['product_id'] + '\',\'' + row['partner_id'] + '\')"></button></div>';
            }
          }
        }
      ]
    });
  });
  
  $.fn.datepicker.defaults.format = "dd-mm-yyyy";
  $('#start_date,#end_date').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      sideBySide: true,
      orientation: 'bottom'
  });

  //search
  $('#search').click(function(e){
    e.preventDefault();
    cloneInput();
    $table.draw();
  });

  function cloneInput() {
    var $box = $("#box");
    var $thinput = $(".thinput").find("input");

    $box.empty();
    $thinput.clone().appendTo("#box").find("input").each(function() {
        this.attr('id','');
        this.attr('type','hidden');
    });
  }

  function getval(sel)
  {
    // alert(sel.value);
    $.post("{{ route('list-product') }}", {
    idbiller 	: sel.value,
    _token		: '{{ Session::token() }}'
  },
  function(data, status){
    // console.log(data);
    $('#product').empty(); 
    $('#product').append($("<option></option>")
                .attr("value", "ALL")
                .text("-- ALL --")); 
    $.each(data, function(key, value) {  
      $('#product')
        .append($("<option></option>")
        .attr("value", key)
        .text(value)); 
      });
    });
  }

  function getvalAdd(sel)
  {
    // alert(sel.value);
    $.post("{{ route('list-product') }}", {
    idbiller 	: sel.value,
    _token		: '{{ Session::token() }}'
  },
  function(data, status){
    // console.log(data);
    $('#productAdd').empty(); 
    $('#productAdd').append($("<option></option>")
                    .attr("value", "")
                    .text("-- Select One --")); 
    $.each(data, function(key, value) {  
      $('#productAdd')
        .append($("<option></option>")
        .attr("value", key)
        .text(value)); 
      });
    });
  }

  $('#add_product').click(function(){
		$.post("{{ route('add-product') }}", {
			billerid	: $('#billerAdd').val(),
			productid : $('#productAdd').val(),
			partnerid : $('#partnerAdd').val(),
			status 	  : $('#statusAdd').val(),
			price 	  : $('#priceAdd').val(),
			tag 	    : 'INSERT',
			_token	  : '{{ Session::token() }}'
		},
		function(data, status){
			if(data === "Your new product already exists.") {
				swal("Failed!", data, "error");
			}else if(data){
				swal("Failed!", "Add New Product", "error");
			} else {
				swal("Success!", "Add New Product", "success");
				// $('#modal-right').modal('hide');
				$table.draw();
			}
        });
  });
  
  $('#edit_product').click(function(){
		$.post("{{ route('add-product') }}", {
			billerid	: $('#billeridedit').val(),
			productid : $('#productidedit').val(),
      partnerid : $('#partneridedit').val(),
      status 	  : $('#statusedit').val(),
			price 	  : $('#priceedit').val(),
			nominal_promo : $('#npromoEdit').val(),
			ispromo 	: $('#statuspromoedit').val(),
			tag 	    : 'UPDATE',
			_token	  : '{{ Session::token() }}'
		},
		function(data, status){
      if(data){
        console.log(data);
				swal("Failed!", "Add Update Product", "error");
			} else {
				swal("Success!", "Add Update Product", "success");
				// $('#modal-right').modal('hide');
				$table.draw();
			}
        });
	});

  function deleteIt(billerID,productID,partnerID) {
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this imaginary file!",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Yes, Delete it!",
			cancelButtonText: "No, Cancel!",
			closeOnConfirm: false,
			closeOnCancel: false
		},
		function(isConfirm) {
			if (isConfirm) {
        // console.log(billerID,productID,partnerID);
				$.post("{{ route('delete-product') }}", {
					billerid		: billerID,
          productid		: productID,
          partnerid		: partnerID,
					_token		: '{{ Session::token() }}'
				},
				function(data, status){
					if(data) {
						swal("Failed!", "Failed Delete Product.", "danger");
					} else {
						swal("Success!", "Delete Product Successfully.", "success");
						$table.draw();
					}
				});
			}
			else{
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
		});
	}

  function editIt(billerID,productID,partnerID) {
		$('#modal-right-edit').modal('show');
		$.post("{{route('detail-product')}}", {
      billerid		: billerID,
      productid		: productID,
      partnerid		: partnerID,
      _token : '{{ Session::token() }}'
		},
		function(data, status){
      // console.log(data);
			$('#billeridedit').val(data.billerid);
      $('#productidedit').val(data.productid);
      $('#partneridedit').val(data.partnerid);
      document.getElementById("billernameedit").innerHTML = data.billername;
      document.getElementById("productnameedit").innerHTML = data.productname;
      document.getElementById("partnernameedit").innerHTML = data.partnername;
			$('#priceedit').val(data.priceedit);
			$('#statuspromoedit').val(data.s_promo);
      $('#npromoEdit').val(data.n_promo);
			$('#statusedit').val(data.isactive);
		});
	}
</script>

