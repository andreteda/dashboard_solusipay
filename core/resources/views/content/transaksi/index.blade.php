<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<style>
	.more {
		background:none;
		border:none;
		cursor:pointer;
	}
</style>
<div class="content-wrapper">
  <section class="content-header">
      <h1>
        {{ $subtitle }}
        <small>{{ $sub_ }}</small>
      </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xl-12 col-12">
        <div class="box bg-teal">
          <div class="box-body">
            <form id="frmFilter" role="form" action="{{ route('export-transaksi') }}" method="get">
              <div class="row">
                <input type="text" value="{{ Auth::user()->roleid }}" name="client" id="client" hidden/>
                  <div class="col-md-2">
                      Start Date:
                      <div class="input-group  input-group-sm date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="start_date" name="start_date"
                              value="{{ date('d-m-Y',strtotime('-1 day')) }}">
                      </div>
                  </div>
                  <div class="col-md-2">
                      End Date:
                      <div class="input-group input-group-sm date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="end_date" name="end_date" value="{{ date("d-m-Y") }}">
                      </div>
                  </div>
                  @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
                  <div class="col-md-2">
                      Partner ID :
                      <div class="input-group input-group-sm">
                      <select name="partner" id="partner" class="form-control">
                          <option value="ALL">-- ALL --</option>
                          @foreach ($data_partner as $item)
                          <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                          @endforeach
                      </select>
                      </div>
                  </div>
                  @endif
              </div>
              <br>
              <div class="row">
                <div class="col-sm-2">
                  TRX ID:
                  <input class="input-group input-group-sm" type="text" name="trxid" id="trxid">
                </div>
                <div class="col-sm-2">
                  Customer ID:
                  <input class="input-group input-group-sm"type="text" name="customerid" id="customerid">
                </div>
                <div class="col-sm-2">
                  Ref:
                  <input class="input-group input-group-sm"type="text" name="ref" id="ref">
                </div>
                <div class="col-sm-2">
                  Product Name:
                  <input class="input-group input-group-sm"type="text" name="productdesc" id="productdesc">
                </div>
                <div class="col-md-1">
                  <br>
                  <button type="button" id="search" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
                <div class="col-md-3">
                  <br>
                  <div class="box-tools pull-right">
                      <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-upload"></i>  Export</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="box">
          <div class="box-body">
            <div class="table-responsive">
              <table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                  <tr>
                    <th> No</th>
                    <th class="text-center">ID Trx</th>
                    <th> Date</th>
                    <th class="text-center">Customer ID</th>
                    <th> Partner</th>
                    <th class="text-center">Product Name</th>
                    <th class="text-center">Ref</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Nominal</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
<script>
  $(function () {
    "use strict";

    $.xhrPool = [];
    $.xhrPool.abortAll = function() {
      $(this).each(function(idx, jqXHR) {
          jqXHR.abort();
      });
      $.xhrPool = [];
    };

    $.ajaxSetup({
      beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
      },
      complete: function(jqXHR) {
        var index = $.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });

  $(document).ready(function () {
    console.log(document.getElementsByName('partner')[0].value);
    $table = $('#datalist').DataTable({
      "processing": true,
      "serverSide": true,
      "dom": "<'top'ip>rt", //"dom": 'Brtip',
      "pageLength" : 10,
      "ordering": false,
      "ajax": {
        "url": "{{ route('load-transaksi') }}",
        "type":"POST",
        "data": function(d){
          if(document.getElementsByName('client')[0].value == '1' || document.getElementsByName('client')[0].value == '2' ){
            d.partnerid = document.getElementsByName('partner')[0].value,
            d.trxid = document.getElementsByName('trxid')[0].value,
            d.customerid = document.getElementsByName('customerid')[0].value,
            d.ref = document.getElementsByName('ref')[0].value,
            d.productdesc = document.getElementsByName('productdesc')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }else{
            d.trxid = document.getElementsByName('trxid')[0].value,
            d.customerid = document.getElementsByName('customerid')[0].value,
            d.ref = document.getElementsByName('ref')[0].value,
            d.productdesc = document.getElementsByName('productdesc')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }
        }
      },
      "columns": [
        { "data": null,"sortable": false,
					mRender: function ( data, type, row, meta ) {
						return meta.row + meta.settings._iDisplayStart + 1;
					} 
				},
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.trxid+ '</div>';
          }
        },
        {  mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.finishdate+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.customerid+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.description+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.productdesc+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.trackingref+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.statuspay+ '</div>';
          }
        },
        { 
          mRender: function (data, type, row) {
            return '<div style="text-align: right;"> ' +row.amount+ '</div>';
          }
        }
      ]
    });
  });
  
  $.fn.datepicker.defaults.format = "dd-mm-yyyy";
  $('#start_date,#end_date').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      sideBySide: true,
      orientation: 'bottom'
  });

  //search
  $('#search').click(function(e){
    e.preventDefault();
    cloneInput();
    $table.draw();
  });

  function cloneInput() {
    var $box = $("#box");
    var $thinput = $(".thinput").find("input");

    $box.empty();
    $thinput.clone().appendTo("#box").find("input").each(function() {
        this.attr('id','');
        this.attr('type','hidden');
    });
  }

</script>

