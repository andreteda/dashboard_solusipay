<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">
<style>
	.more {
		background:white;
		border:none;
		cursor:pointer;
    outline: none;
    border: none;
    border-radius: 15px;
    width: 100px;
  }
    
  .morered {
		background:white;
		border:none;
		cursor:pointer;
    outline: none;
    border: none;
    border-radius: 15px;
    width: 100px;
	}

  .more:hover {
      border: 2px solid #4CAF50;
  }

  .more:active {
      background-color: #3e8e41;
  }

  .morered:hover {
      border: 2px solid red;
  }

  .morered:active {
      background-color: #3e8e41;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
      <h1>
        {{ $subtitle }}
        <small>{{ $sub_ }}</small>
      </h1>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xl-12 col-12">
        <div class="box bg-teal">
          <div class="box-body">
            <form id="frmFilter" role="form" action="{{ route('export-transaksi') }}" method="get">
              <div class="row">
                  <input type="text" value="{{ $clientID }}" name="client" id="client" hidden/>
                  <div class="col-md-2">
                      Start Date:
                      <div class="input-group  input-group-sm date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="start_date" name="start_date"
                              value="{{ date('d-m-Y',strtotime('-1 day')) }}">
                      </div>
                  </div>
                  <div class="col-md-2">
                      End Date:
                      <div class="input-group input-group-sm date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="end_date" name="end_date" value="{{ date("d-m-Y") }}">
                      </div>
                  </div>
                  @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
                  <div class="col-md-2">
                      Partner ID :
                      <div class="input-group input-group-sm">
                      <select name="partner" id="partner" class="form-control">
                          <option value="ALL">-- ALL --</option>
                          @foreach ($data_partner as $item)
                          <option value="{{ $item->partnerid }}"> {{ $item->description  }}</option>
                          @endforeach
                      </select>
                      </div>
                  </div>
                  @endif
              </div>
              <br>
              <div class="row">
                <div class="col-sm-2">
                    Order ID:
                    <input class="input-group input-group-sm" type="text" name="orderid" id="orderid">
                </div>
                <div class="col-sm-2">
                    Tracking Reff:
                    <input class="input-group input-group-sm" type="text" name="trackingref" id="trackingref">
                </div>
                <div class="col-sm-2">
                    Book ID:
                    <input class="input-group input-group-sm" maxlength="1" size="1" type="text" name="bookid" id="bookid">
                </div>
                <div class="col-md-1">
                    <br>
                    <button type="button" id="search" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-search"></i> Search</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="box">
          <div class="box-body">
            <div class="table-responsive">
              <table id="datalist" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                <thead>
                  <tr>
                    <th class="text-center">Order ID</th>
                    <th class="text-center">Tracking Ref</th>
                    <th class="text-center">Book ID</th>
                    <th class="text-right">Amount</th>
                    <th class="text-center">Created On</th>
                    <th class="text-center">Action</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
	</section>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
<script>
  $(function () {
    "use strict";

    $.xhrPool = [];
    $.xhrPool.abortAll = function() {
      $(this).each(function(idx, jqXHR) {
          jqXHR.abort();
      });
      $.xhrPool = [];
    };

    $.ajaxSetup({
      beforeSend: function(jqXHR) {
        $.xhrPool.push(jqXHR);
      },
      complete: function(jqXHR) {
        var index = $.xhrPool.indexOf(jqXHR);
        if (index > -1) {
            $.xhrPool.splice(index, 1);
        }
      },
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  });

  $(document).ready(function () {
    $table = $('#datalist').DataTable({
      "processing": true,
      "serverSide": true,
      "dom": "<'top'ip>rt",
      "pageLength" : 10,
      "ordering": false,
      "ajax": {
        "url": "{{ route('load-suspecttrx') }}",
        "type":"POST",
        "data": function(d){
          if(document.getElementsByName('client')[0].value == "SA"){
            d.partnerid = document.getElementsByName('partner')[0].value,
            d.orderid = document.getElementsByName('orderid')[0].value,
            d.trackingref = document.getElementsByName('trackingref')[0].value,
            d.bookid = document.getElementsByName('bookid')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }else{
            d.partner = '',
            d.orderid = document.getElementsByName('orderid')[0].value,
            d.trackingref = document.getElementsByName('trackingref')[0].value,
            d.bookid = document.getElementsByName('bookid')[0].value,
            d.start_date = document.getElementsByName('start_date')[0].value,
            d.end_date = document.getElementsByName('end_date')[0].value,
            d._token = '{{ Session::token() }}'
          }
        }
      },
      "columns": [
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.orderid+ '</div>';
          }
        },
        {  mRender: function (data, type, row) {
            return '<div style="text-align: left;"> ' +row.trackingref+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.bookid+ '</div>';
          }
        },
        { 
          mRender: function (data, type, row) {
            return '<div style="text-align: right;"> ' +row.amount+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"> ' +row.finishdate+ '</div>';
          }
        },
        {  
          mRender: function (data, type, row) {
            return '<div style="text-align: center;"><button class="more glyphicon glyphicon-ok" style="color:green" onclick="approveIt(\'' + row['orderid'] + '\')"> SUCCESS</button><button class="morered glyphicon glyphicon-repeat" style="color:red" onclick="refundIt(\'' + row['orderid'] + '\')"> REFUND</button></div>';
          }
        }
      ]
    });
  });
  
  $.fn.datepicker.defaults.format = "dd-mm-yyyy";
  $('#start_date,#end_date').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
      sideBySide: true,
      orientation: 'bottom'
  });

  //search
  $('#search').click(function(e){
    e.preventDefault();
    cloneInput();
    $table.draw();
  });

  function cloneInput() {
    var $box = $("#box");
    var $thinput = $(".thinput").find("input");

    $box.empty();
    $thinput.clone().appendTo("#box").find("input").each(function() {
        this.attr('id','');
        this.attr('type','hidden');
    });
  }

  function approveIt(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Force Success it!",
        cancelButtonText: "No, Cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.post("{{ route('force-success') }}", {
                orderid   : id,
                _token		: '{{ Session::token() }}'
            },
            function(data){
                if(data) {
                    // console.log(data);
                    swal("Failed!", data , "error");
                } else {
                    swal({
                        title: "Success!",
                        text: "Force success is successfully.",
                        type: "success"},
                        function(){
                          $table.draw();
                        }
                    );
                }
            });
        }else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });
  }

  function refundIt(id) {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, Refund it!",
        cancelButtonText: "No, Cancel!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
        if (isConfirm) {
            $.post("{{ route('refund') }}", {
                orderid   : id,
                _token		: '{{ Session::token() }}'
            },
            function(data){
                if(data) {
                    // console.log(data);
                    swal("Failed!", data , "error");
                } else {
                    swal({
                        title: "Success!",
                        text: "Refund is successfully.",
                        type: "success"},
                        function(){
                          $table.draw();
                        }
                    );
                }
            });
        }else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });
  }

</script>

