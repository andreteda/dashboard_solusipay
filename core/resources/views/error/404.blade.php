
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <title>404 Page not found </title>
    <link rel="stylesheet" href="{{ asset('../assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/bootstrap-extend.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}"> 
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}">   
</head>
<body class="hold-transition">
    <div class="error-body">
        <div class="error-page">
            <div class="error-content">
                <div class="container">
                    <h2 class="headline text-yellow"> 404</h2>  
                    <h3 class="margin-top-0"><i class="fa fa-warning text-yellow"></i> PAGE NOT FOUND !</h3>
                    <p>YOU DON'T HAVE ANY PERMISSION TO ACCESS THIS SITE !!!</p>
                    <div class="text-center">
                        <a href="{{ route('dashboard') }}" class="btn btn-info btn-block margin-top-10">Back to dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
</body>
</html>
