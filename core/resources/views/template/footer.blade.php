		<footer class="main-footer">
   			 &copy; 2020 PT. Inovasi Daya Solusi. All Rights Reserved.
  		</footer>

        <!-- Modal -->
        <div class="modal center-modal fade" id="modal-center" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Session expired</h5>
                    </div>
                    <div class="modal-body">
                        <p>Your session has expired. Would you like to be redirected to the login page?</p>
                    </div>
                    <div class="modal-footer modal-footer-uniform">
                        <button type="button" class="btn btn-bold btn-pure btn-primary float-right" id="gotologin">Go To Login</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
	</div>
</html>
  {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.js') }}"></script>--}}
  {{--<script src="http://www.amcharts.com/lib/3/amcharts.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/gauge.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/serial.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/amstock.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/pie.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/plugins/animate/animate.min.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/plugins/export/export.min.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/themes/patterns.js" type="text/javascript"></script>
  <script src="http://www.amcharts.com/lib/3/themes/light.js" type="text/javascript"></script>--}}
  <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/select2/dist/js/select2.full.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/iCheck/icheck.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/Web-Ticker-master/jquery.webticker.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/echarts-master/dist/echarts-en.min.js') }}"></script>
  <script src="{{ asset('assets/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js') }}"></script>
  <script src="{{ asset('js/template.js') }}"></script>
  <script src="{{ asset('js/pages/dashboard.js') }}"></script>
  <script src="{{ asset('js/pages/dashboard-chart.js') }}"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/pages/advanced-form-element.js') }}"></script>
  <script src="{{ asset('assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
  <script>
      $(document).ready(function() {
          function checkSesion() {
              myVar = setInterval(function() {
                  $.ajax({
                      method: 'GET',
                      url: "{{ route('check') }}",
                      data: {},
                      success: function (data, status) {
                          //console.log(status);
                          if (data) {
                              var datanotif = JSON.stringify(data);
                              //console.log(datanotif);
                          }
                      },
                      error: function (xhr, ajaxOptions, thrownError) {
                          if (xhr.status == 401) {
                              $('#modal-center').modal({
                                  backdrop: 'static',
                                  keyboard: false
                              }).show();

                              $("#gotologin").click(function () {
                                  location.reload(true);
                              });
                          }
                      }
                  });
              }, 7199000);
          }

          checkSesion();
      });
  </script>
</body>
</html>