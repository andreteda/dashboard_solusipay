<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <title>Solusipay Admin</title>
    <link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-extend.css') }}">
    <link rel="stylesheet" href="{{ asset('css/master_style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.css') }}"> 
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
          <img src="{{ asset('images/solusipay/ids.png') }}" width="50%">
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            	<form class="form-element" method="post" action="login-v2.html" autocomplete="off">
                  	<div class="form-group has-feedback">
                      	<input type="email" class="form-control" placeholder="Email" id="email">
                      	<span class="ion ion-email form-control-feedback"></span>
                  	</div>
                  	<div class="form-group has-feedback">
                      	<input type="password" class="form-control" placeholder="Password" id="password" onkeypress="return runScript(event)">
                      	<span class="ion ion-locked form-control-feedback"></span>
                  	</div>
                  	<div class="row">
                      	<div class="col-12 text-center">
                          	<button type="button" id="login" class="btn btn-success btn-block margin-top-10">SIGN IN</button>
                      	</div>
                  	</div>
              	</form>
              	<div class="margin-top-30 text-center">
                  	<p>Don't have an account? <a href="#" onclick="notice_reg()" class="text-info m-l-5">Sign Up</a></p>
                  	<p>Loss your password? <a href="#" onclick="notice_forgot()" class="text-info m-l-5">Reset Password</a></p>
              	</div>
        </div>
    </div>
    <!-- <script src="{{ asset('assets/vendor_components/jquery/dist/jquery.min.js') }}"></script> -->
    <script src="{{ asset('assets/vendor_components/popper/dist/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
    <script>
        (function(document, window, $) {
          'use strict';
          var Site = window.Site;
          $(document).ready(function() {
            // Site.run();
          });
        })(document, window, jQuery);
        function runScript(e) {
          if (e.keyCode == 13) {
            doLogin();
          }
        }
        function notice_reg(){
            swal("Info!", 'Contact Solusipay Admin to register!', "warning");
        }
        function notice_forgot(){
            swal("Info!", 'Contact Solusipay Admin to reset password!', "warning");
        }
        function doLogin() {
          $.post("{{ route('do-login') }}", {
                username  : $('#email').val(),
                password  : $('#password').val(),
                _token    : '{{ Session::token() }}'
            },
            function(data, status){
                if(data) {
                    var datanotif = JSON.parse(data);
                      if(datanotif.status == '200'){
                          window.location.replace("{{route('dashboard')}}");
                      } else {
                          if(datanotif.status == 'token_error') {
                              swal("Token Expired", datanotif.messages, "login").then(function() {
                                  window.location.reload(true);
                              });
                          } else {
                              swal("Error!", datanotif.messages, "error");
                          }
                      }
                }
            });
        }
        $('#login').click(function(){
            doLogin();
        });
    </script>
</body>
</html>
