<style>
  .sidebar-menu {
    max-height: calc(96.79vh - 16rem);
    overflow: hidden;
  }

  .sidebar-menu:hover {
    overflow-y: auto;
  }

  ::-webkit-scrollbar {
    width: 10px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #252525;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #888;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #a1a1a1;
  }

</style>


<aside class="main-sidebar">
  <section class="sidebar">
    <div class="user-panel">
      <div class="image">
        <img src="{{ asset('images/avatar/avatar_1.png') }}" class="rounded-circle" alt="User Image">
      </div>
      <div class="info">
        <p>{{ Auth::user()->username }}</p>
        <p>{{ $data_user->role_desc }}</p>
      </div>
    </div>
    <ul class="sidebar-menu" data-widget="tree">
        <li class="{{ request()->is('/') ? 'active' : '' }}">
          <a href="{{ route('dashboard') }}">
            <i class="icon-home"></i> <span>Home</span>
          </a>
        </li>
        <li class="{{ request()->is('transaksi') ? 'active' : '' }}">
          <a href="{{ route('transaksi') }}">
            <i class="icon-chart {{ request()->is('transaksi*') ? 'active' : '' }}"></i> <span>Transaksi</span>
          </a>
        </li>
      @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
        <li class="{{ request()->is('suspect-transaksi') ? 'active' : '' }}">
          <a href="{{ route('suspect-transaksi') }}">
            <i class="glyphicon glyphicon-random {{ request()->is('suspect-transaksi*') ? 'active' : '' }}"></i> <span>Suspect Transaksi</span>
          </a>
        </li>
      @endif
      @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
        <li class="{{ request()->is('rekon-transaksi') ? 'active' : '' }}">
          <a href="{{ route('rekon-transaksi') }}">
            <i class="fa fa-rocket"></i> <span>Rekon</span>
          </a>
        </li>
      @endif
        <li class="{{ request()->is('product') ? 'active' : '' }}">
          <a href="{{ route('product') }}">
            <i class="glyphicon glyphicon-thumbs-up {{ request()->is('product*') ? 'active' : '' }}"></i> <span>Product</span>
          </a>
        </li>
      @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
        <li class="{{ request()->is('payment-method') ? 'active' : '' }}">
          <a href="{{ route('payment-method') }}">
            <i class="glyphicon glyphicon-credit-card"></i> <span>Payment Method</span>
          </a>
        </li>
      @endif
      @if(Auth::user()->roleid == 1 || Auth::user()->roleid == 2)
        <li class="treeview {{ request()->is('user/*') ? 'active menu-open' : '' }}">
          <a href="#">
            <i class="icon-settings"></i>
            <span>Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ request()->is('user/user-list') ? 'active' : '' }}"><a href="{{ route('user-list') }}">List
                User</a></li>
            @if(Auth::user()->roleid == 1)
            <li class="{{ request()->is('user/user-level') ? 'active' : '' }}"><a href="{{ route('user-level') }}">User
                Level</a></li>
            @endif
          </ul>
        </li>
      @endif
        <li class="{{ request()->is('chpass') ? 'active' : '' }}">
          <a href="{{ route('chpass') }}">
            <i class="glyphicon glyphicon-pencil"></i> <span>Change Password</span>
          </a>
        </li>
    </ul>
  </section>
</aside>
