<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use DB;
use Auth;
class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = '')
    {
        $aksesref2 = [];
        $dataSet = DB::table('tb_m_user_role')->get();
        foreach ($dataSet as $d) {
            $role = $d->role_name . "-" . $role;
            $aksesref2[$d->role_desc] = $d->role_name ;
        }
        
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('auth/login');
            }
        }else {
			$data = DB::table('tb_m_user_solusi')
                    ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
                    ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
                    ->first();
			$akses = explode('-',$role);

			if(!empty($role)){
				if(in_array($aksesref2[$data->role_desc],$akses)){
					$roleCheck = true;
				}else{
					$roleCheck = false;
				}
			}else{
				$roleCheck = true;
			}

            if ($roleCheck) {
                return $next($request);
            } else {
                if ($request->ajax()) {
                    return response('Forbidden.', 403);
                } else {
                    return response()->view('error.404', [], 404);
                }
            }
        }
    }
}
