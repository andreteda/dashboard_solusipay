<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Input;
use Hash;
use Crypt;

class CoreController extends Controller {
    public static function getDataUser() {
        $dataUser = DB::table('tb_m_user_solusi')
                ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
                ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
                ->first();
        return $dataUser;
    }

    public static function getDataPartner()
    {
        $dataPartner = DB::table('tb_m_partner')
            ->where('tb_m_partner.isactive', '=', '1')
            ->get();
        return $dataPartner;
    }

    public function showDashboard() {
        if(empty(Auth::user()->id)) {
            return redirect(route('login'));
        } else {
            $data['data_user'] = $this::getDataUser();
            $data['data_partner'] = $this::getDataPartner();
            $data['title'] = 'Dashboard';
            $data['subtitle'] = 'Main Dashboard';
            $data['page'] = 'content.dashboard';

            return view('template.content')->with($data);
        }
    }

    public function showLogin() {
        return view('template.login')->with('data', 'dashboard');
    }

    // Testing Loginv2
    public function showLoginv2() {
        return view('template.loginv2')->with('data', 'dashboard');
    }

    public function doLogout() {
        Auth::logout();
        return redirect(route('login'));
    }

    public function doLogin(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return $validator->messages()->toJson();
        }
        $username = $request->username;
        $password = $request->password;
        $isactive = DB::table('tb_m_user_solusi')->where('userid', '=', $request->username)->get();

        if (Auth::attempt(['email' => $username, 'password' => $password])) {
            if ($isactive[0]->isactive == "1"){
                $data = array('status' => '200', 'messages' => 'Login Success.');
                return json_encode($data);
            }else{
                $data = array('status' => 'Ups', 'messages' => 'Your email is non-active.');
                return json_encode($data);
            }
        } else {
            $data = array('status' => 'Ups', 'messages' => 'Email and password doesn\'t match.');
            return json_encode($data);
        }
    }

    public function changePass(Request $request) {
        $user = UserModel::find(Auth::user()->userid);
        $rules = array(
            'current_password' => 'required',
            'new_password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return $validator->messages()->toJson();
        } else {
            if (!Hash::check($request->current_password, $user->user_password)) {
                $data = array('messages' => 'Your old password is incorrect');
                return json_encode($data);
            } else {
                $user->user_password = bcrypt($request->new_password);
                $user->save();
            }
        }

    }

    /**
     * @param Store $session
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Store $session) {
        if(Auth::check()) {
            return response()->json(['type' => 'success', 'message' => ['name' => Auth::user()->name,
                'time' => time()]
            ], 200);
        } else {
            Auth::logout();
            return response()->json(['type' => 'warning', 'message' => 'Session expired'], 401);
        }
    }
}
