<?php

namespace App\Http\Controllers;

ini_set("default_charset", 'utf-8');
ini_set('max_execution_time', 0);
ini_set('memory_limit', '1024M');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;
use Crypt;
use Excel;
use App\Models\ConfigModel;
use Khill\Lavacharts\Lavacharts;
use Mail;

class APIController extends Controller
{
  var $dataUser = false;

  public function __construct()
  {
    $this->dataUser = $this->getDataUser();
    $this->clientId = strtoupper($this->dataUser->role_name);
  }

  public static function getDataUser()
  {
    $dataUser = DB::table('tb_m_user_solusi')
      ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
      ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
      ->first();
    return $dataUser;
  }

  // About-User
  public function apiDataRole()
  {
    $db = ConfigModel::getListRole();
    return json_encode($db);
  }

  public function apiAddRole(Request $req)
  {
    DB::table('tb_m_user_role')->insert([
      'role_name' => $req->role_code,
      'role_desc' => $req->role_name,
    ]);
  }

  public function apiDeleteRole(Request $req)
  {
    DB::table('tb_m_user_role')->where('role_id', '=', $req->role_id)->delete();
  }

  public function apiDataUser()
  {
    $db = ConfigModel::getDataUser();
    return json_encode($db);
  }

  public function apiAddUser(Request $req)
  {
    if($req->tag == "INSERT") {
      $userid = DB::table('tb_m_user_solusi')->where('userid', '=', $req->email)->get();
      if ($userid[0]->userid){
          return "Your email already taken.";
      }else{
        DB::table('tb_m_user_solusi')->insert([
          'username' => $req->name,
          'userid' => $req->email,
          'email' => $req->email,
          'password' => bcrypt($req->password),
          'isactive' => $req->status,
          'createdon' => date('Y-m-d H:i:s'),
          'roleid' => $req->role,
        ]);
      }
    }else{
      $update = DB::table('tb_m_user_solusi')->where('id', '=', $req->id)->update([
          'mobile_phone' => $req->phone,
          'roleid' => $req->role,
          'isactive' => $req->status
      ]);

      $data = array('status' => '200', 'messages' => 'Success Update User.');
      return json_encode($data);
    }
  }

  public function apiDeleteUser(Request $req)
  {
    DB::table('tb_m_user_solusi')->where('id', '=', $req->role_id)->delete();
  }

  public function apiDetailUser(Request $req)
  {
    $datax = DB::table('tb_m_user_solusi')->where('id', '=', $req->id)->first();

    $data['id'] = $datax->id;
    $data['username'] = $datax->username;
    $data['phone'] = $datax->mobile_phone;
    $data['role'] = $datax->roleid;
    $data['status'] = $datax->isactive;

    return $data;
  }

  public function chPass(Request $req){
    $oldpassDB = DB::table('tb_m_user_solusi')
                ->where('id',$req->role_id)
                ->first();
    if (Hash::check($req->oldpass, Auth::user()->password)) {
        // Jika password benar
        $sqlUpdate = DB::table('tb_m_user_solusi')
                    ->where('id',$req->role_id)
                    ->update([
                        'password' => bcrypt($req->newpass)
                    ]);
    }else{
        // jika password tidak sesuai
        return "false";
    }
  }
  // END About-User

  public function apiProduct(Request $req)
  {
    $db = DB::table('tb_m_biller_solusi')
        ->select('Productid','Productdesc')
        ->where('tb_m_biller_solusi.billerid', '=', $req->idbiller)
        ->where('tb_m_biller_solusi.isactive', '=', '1')
        ->get();
    foreach ($db as $d) {
      $dataProduct[$d->Productid] = $d->Productdesc ;
    }
    // dd(json_encode($dataProduct));
    return $dataProduct;
  }

  public function apiDataProduct(Request $req)
  {
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $partnerid = $this->clientId;
    }else{
      $partnerid = $req->partnerid;
    }

    $start = $req->start;
    $length = $req->length;
    $draw = $req->draw;
    $order = $req->order;
    $where = $this->ParamsDataProduct($req);
    $type = '';
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    if(!empty($order)){
      $type = $order[0]['dir'];
      $order = $req->columns[$order[0]['column']]['data'];
    }

    $data = $this->getDataProduct($where,$start,$length,$order,$type,$req->partnerid);

    $total = DB::connection('mysql')->table('tb_m_biller_solusi_detail_partner')
      ->whereRaw("tb_m_biller_solusi_detail_partner.createdon between '" .$start_date. "' and '" .$end_date. "' and ((tb_m_biller_solusi_detail_partner.partnerid = '".$partnerid."')OR('ALL' = '".$partnerid."'))")
      ->count();

    $filtered = DB::connection('mysql')
      ->table('tb_m_biller_solusi_detail_partner')
      ->whereRaw($where)
      ->whereRaw("((tb_m_biller_solusi_detail_partner.partnerid = '".$partnerid."') OR ('ALL' = '".$partnerid."'))")
      ->count();

    $output = [
      'draw' => (int) $draw,
      'recordsTotal' => $total,
      'recordsFiltered' => $filtered,
      'data' => $data
    ];

    return json_encode($output);
  }

  public function ParamsDataProduct(Request $req){
    $param = [];
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    $start_date_e = date('Y-m-d'). ' 00:00:00';
    $end_date_e = date('Y-m-d'). ' 23:59:59';
    if(!empty($req->biller)) array_push($param, '((tb_m_biller_solusi_detail_partner.Billerid = \'' .$req->biller. '\') OR (\''.$req->biller.'\' = "ALL"))');
    if(!empty($req->product)) array_push($param, '((tb_m_biller_solusi_detail_partner.Productid = \'' .$req->product. '\') OR (\''.$req->product.'\' = "ALL"))');
    if(!empty($start_date)&& !empty($end_date)) array_push($param , "tb_m_biller_solusi_detail_partner.createdon between '" .$start_date. "' and '" .$end_date. "'");
    if(empty($start_date) || empty($end_date)) array_push($param , "tb_m_biller_solusi_detail_partner.createdon between '" .$start_date_e. "' and '" .$end_date_e. "'");

    if(count($param) > 0) {
      $where = implode(' and ',$param);
    } else {
      $where = "1";
    }

    return $where;
  }

  public function getDataProduct($where, $start = null, $length = null,$order = null,$type = null,$partnerid){
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $where = "tb_m_biller_solusi_detail_partner.partnerid = '$this->clientId' and " . $where;
    }else{
      if($partnerid <> "ALL"){
        $where = "tb_m_biller_solusi_detail_partner.partnerid = '$partnerid' and " . $where;
      }
    }

    $dataSet = DB::connection('mysql')
                ->table('tb_m_biller_solusi_detail_partner')
                ->join('tb_m_biller', 'tb_m_biller.biller_id', '=', 'tb_m_biller_solusi_detail_partner.Billerid')
                ->join('tb_m_partner', 'tb_m_partner.partnerid', '=', 'tb_m_biller_solusi_detail_partner.partnerid')
                ->join('tb_m_biller_solusi', function($join){
                    $join->on('tb_m_biller_solusi_detail_partner.billerid', '=', 'tb_m_biller_solusi.Billerid');
                    $join->on('tb_m_biller_solusi_detail_partner.Productid','=','tb_m_biller_solusi.Productid');
                  })
                ->selectraw('tb_m_biller.biller_name,tb_m_biller_solusi.ProductDesc,tb_m_partner.description,tb_m_biller_solusi_detail_partner.isactive,tb_m_biller_solusi_detail_partner.nominal,tb_m_biller_solusi_detail_partner.createdon,tb_m_biller_solusi_detail_partner.Billerid,tb_m_biller_solusi_detail_partner.Productid,tb_m_biller_solusi_detail_partner.partnerid')
                ->whereRaw($where);

    if(!empty($length))
      $dataSet->take($length);
    if(!empty($length))
      $dataSet->skip($start);
    if(empty($order))
      $dataSet->orderBy('tb_m_biller.biller_name','Asc')
              ->orderBy('tb_m_biller_solusi.ProductDesc','Asc')
              ->orderBy('tb_m_partner.description','Asc')
              ->orderBy('tb_m_biller_solusi_detail_partner.createdon','Desc');
    else {

    }

    $data = [];
    $token = null;
    foreach ($dataSet->get() as $d) {
      $data[] = [
        'biller_id' => $d->Billerid,
        'biller_name' =>$d->biller_name,
        'product_id' => $d->Productid,
        'ProductDesc' => $d->ProductDesc,
        'partner_id' => $d->partnerid,
        'description' => $d->description,
        'isactive' => $d->isactive,
        'nominal' => number_format($d->nominal,0),
        'createdon' => $d->createdon
      ];
    }

    return $data;
  }

  public function apiAddProduct(Request $req){
    if($req->tag == "INSERT") {
      $dataProduct = DB::table('tb_m_biller_solusi_detail_partner')
                  ->where('Billerid', '=', $req->billerid)
                  ->where('Productid', '=', $req->productid)
                  ->where('partnerid', '=', $req->partnerid)
                  ->get();
      if ($dataProduct[0]->createdby){
          return "Your new product already exists.";
      }else{
        DB::table('tb_m_biller_solusi_detail_partner')->insert([
          'Billerid' => $req->billerid,
          'Productid' => $req->productid,
          'partnerid' => $req->partnerid,
          'isactive' => $req->status,
          'nominal' => $req->price,
          'nominal_spromo' => 0,
          'ispromo' => 0,
          'createdon' => date('Y-m-d H:i:s'),
          'createdby' => $this->dataUser->username,
        ]);
      }
    }else{
      $update = DB::table('tb_m_biller_solusi_detail_partner')
                  ->where('Billerid', '=', $req->billerid)
                  ->where('Productid', '=', $req->productid)
                  ->where('partnerid', '=', $req->partnerid)
                  ->update([
                    'nominal' => $req->price,
                    'nominal_spromo' => $req->nominal_promo,
                    'ispromo' => $req->ispromo,
                    'isactive' => $req->status
                  ]);
    }
  }

  public function apiDeleteProduct(Request $req){
    DB::table('tb_m_biller_solusi_detail_partner')
      ->where('Billerid', '=', $req->billerid)
      ->where('Productid', '=', $req->productid)
      ->where('partnerid', '=', $req->partnerid)
      ->delete();
  }

  public function apiDetailProduct(Request $req)
  {
    $datax = DB::table('tb_m_biller_solusi_detail_partner')
                ->join('tb_m_biller', 'tb_m_biller.biller_id', '=', 'tb_m_biller_solusi_detail_partner.Billerid')
                ->join('tb_m_partner', 'tb_m_partner.partnerid', '=', 'tb_m_biller_solusi_detail_partner.partnerid')
                ->join('tb_m_biller_solusi', function($join){
                    $join->on('tb_m_biller_solusi_detail_partner.billerid', '=', 'tb_m_biller_solusi.Billerid');
                    $join->on('tb_m_biller_solusi_detail_partner.Productid','=','tb_m_biller_solusi.Productid');
                  })
                ->selectraw('tb_m_biller.biller_name,tb_m_biller_solusi.ProductDesc,tb_m_partner.description,tb_m_biller_solusi_detail_partner.isactive,tb_m_biller_solusi_detail_partner.nominal,tb_m_biller_solusi_detail_partner.createdon,tb_m_biller_solusi_detail_partner.Billerid,tb_m_biller_solusi_detail_partner.Productid,tb_m_biller_solusi_detail_partner.partnerid,tb_m_biller_solusi_detail_partner.ispromo,tb_m_biller_solusi_detail_partner.nominal_spromo')
                ->where('tb_m_biller_solusi_detail_partner.Billerid', '=', $req->billerid)
                ->where('tb_m_biller_solusi_detail_partner.Productid', '=', $req->productid)
                ->where('tb_m_biller_solusi_detail_partner.partnerid', '=', $req->partnerid)
                ->first();

    $data['billerid'] = $datax->Billerid;
    $data['billername'] = $datax->biller_name;
    $data['productid'] = $datax->Productid;
    $data['productname'] = $datax->ProductDesc;
    $data['partnerid'] = $datax->partnerid;
    $data['partnername'] = $datax->description;
    $data['isactive'] = $datax->isactive;
    $data['priceedit'] = $datax->nominal;
    $data['createdon'] = $datax->createdon;
    $data['s_promo'] = $datax->ispromo;
    $data['n_promo'] = $datax->nominal_spromo;

    return $data;
  }

  // Transaksi
  public function apiTransaksi(Request $req) {

    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $partnerid = $this->clientId;
    }else{
      $partnerid = $req->partnerid;
    }
    // dd($this->clientId,$partnerid,$req->partnerid);
    $start = $req->start;
    $length = $req->length;
    $draw = $req->draw;
    $order = $req->order;
    $where = $this->storeParamsTransaksi($req);
    $type = '';
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    $data = $this->getDataTransaksi($where,$start,$length,$order,$type,$req->partnerid);

    $total = DB::connection('mysql')->table('tb_r_solusiorder')
      ->whereRaw("finishdate between '" .$start_date. "' and '" .$end_date. "'")
      ->count();

    $filtered = DB::connection('mysql')
      ->table('tb_r_solusiorder')
      ->whereRaw($where)
      ->whereRaw("((tb_r_solusiorder.partnerid = '".$partnerid."') OR ('ALL' = '".$partnerid."'))")
      ->count();

    $output = [
      'draw' => (int) $draw,
      'recordsTotal' => $total,
      'recordsFiltered' => $filtered,
      'data' => $data
    ];

    return json_encode($output);
  }

  public function storeParamsTransaksi(Request $req){
    $param = [];
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    $start_date_e = date('Y-m-d'). ' 00:00:00';
    $end_date_e = date('Y-m-d'). ' 23:59:59';
    if(!empty($req->trxid)) array_push($param, 'tb_r_solusiorder.Trxid LIKE \'%' .$req->trxid. '%\'');
    if(!empty($req->customerid)) array_push($param, 'tb_r_solusiorder.Customerid LIKE \'%' .$req->customerid. '%\'');
    if(!empty($req->ref)) array_push($param, 'tb_r_solusiorder.trackingref LIKE \'%' .$req->ref. '%\'');
    if(!empty($req->productdesc)) array_push($param, 'tb_m_biller_solusi.Productdesc LIKE \'%' .$req->productdesc. '%\'');
    if(!empty($start_date)&& !empty($end_date)) array_push($param , "tb_r_solusiorder.createdon between '" .$start_date. "' and '" .$end_date. "'");
    if(empty($start_date) || empty($end_date)) array_push($param , "tb_r_solusiorder.createdon between '" .$start_date_e. "' and '" .$end_date_e. "'");

    if(count($param) > 0) {
      $where = implode(' and ',$param);
    } else {
      $where = "1";
    }

    return $where;
  }

  function getDataTransaksi($where, $start = null, $length = null,$order = null,$type = null,$partnerid){

    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $where = "tb_r_solusiorder.partnerid = '$this->clientId' and " . $where;
    }else{
      if($partnerid <> "ALL"){
        $where = "tb_r_solusiorder.partnerid = '$partnerid' and " . $where;
      }
    }

    $dataSet = DB::table('tb_r_solusiorder')
                  ->join('tb_m_biller_solusi_detail_partner', function($join){
                      $join->on('tb_m_biller_solusi_detail_partner.Billerid','=','tb_r_solusiorder.billerid');
                      $join->on('tb_m_biller_solusi_detail_partner.Productid','=','tb_r_solusiorder.productid');
                      $join->on('tb_m_biller_solusi_detail_partner.partnerid','=','tb_r_solusiorder.partnerid');
                  })
                  ->join('tb_m_biller_solusi', function($join){
                    $join->on('tb_m_biller_solusi.Billerid','=','tb_m_biller_solusi_detail_partner.Billerid');
                    $join->on('tb_m_biller_solusi.Productid','=','tb_m_biller_solusi_detail_partner.Productid');
                  })
                  ->join('tb_m_partner','tb_m_partner.partnerid','=','tb_r_solusiorder.Partnerid')
                  ->selectraw('CASE WHEN tb_r_solusiorder.Trxid IS NULL THEN "" ELSE tb_r_solusiorder.Trxid END as Trxid,tb_r_solusiorder.finishdate,tb_r_solusiorder.Customerid,tb_m_partner.description,tb_m_biller_solusi.Productdesc,tb_r_solusiorder.trackingref,tb_r_solusiorder.statuspay,tb_r_solusiorder.Amount')
                  ->whereRaw($where);

    if(!empty($length))
      $dataSet->take($length);
    if(!empty($length))
      $dataSet->skip($start);
    if(empty($order))
      $dataSet->orderBy('tb_r_solusiorder.finishdate','desc');
    else {

    }

    $data = [];
    $token = null;
    foreach ($dataSet->get() as $d) {
      $data[] = [
        'trxid' =>$d->Trxid,
        'finishdate' => $d->finishdate,
        'customerid' => $d->Customerid,
        'description' => $d->description,
        'productdesc' => $d->Productdesc,
        'trackingref' => $d->trackingref,
        'statuspay' => $d->statuspay,
        'amount' =>number_format($d->amount, 0)
      ];
    }

    return $data;
  }

  public function apiExportTransaksi(Request $req) {
    $start = "";
    $length = "";
    $draw = "";
    $order = "";
    $param = $this->storeParamsTransaksi($req);
    $list = $this->getDataTransaksi($param,$start,$length,$order,$type,$req->partner);
    $header = array('TrxID','Date','Customer ID','Partner','Product Name','Ref','Status','Nominal');
    
    Excel::create('export_transaksi', function($excel) use($list,$header) {
      $excel->sheet('Sheet1', function($sheet) use($list,$header) {
        $sheet->prependRow(1, $header);
        $sheet->cells('A1:K1', function ($cells) {
          $cells->setFontSize(12);
          $cells->setBackground('#217346');
        });
        $sheet->setColumnFormat(array(
          'A:F' => '@'
        ));

        $sheet->fromArray($list,null,'A2',null,false);
      });
    })->export('xlsx');
  }

  public function apiSuspectTransaksi(Request $req){
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $partnerid = $this->clientId;
    }else{
      $partnerid = $req->partnerid;
    }

    $start = $req->start;
    $length = $req->length;
    $draw = $req->draw;
    $order = $req->order;
    $where = $this->storeParamsSuspectTrx($req);
    $type = '';
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    $data = $this->getDataSuspectTrx($where,$start,$length,$order,$type,$req->partnerid);

    $total = DB::connection('mysql')->table('tb_r_solusiorder')
      ->whereRaw("finishdate between '" .$start_date. "' and '" .$end_date. "'")
      ->whereRaw('tb_r_solusiorder.isfinish = "2" AND tb_r_solusiorder.statuspay = "SUCCESS"')
      ->count();

    $filtered = DB::connection('mysql')
      ->table('tb_r_solusiorder')
      ->whereRaw('tb_r_solusiorder.isfinish = "2" AND tb_r_solusiorder.statuspay = "SUCCESS"')
      ->whereRaw($where)
      ->whereRaw("((tb_r_solusiorder.partnerid = '".$partnerid."') OR ('ALL' = '".$partnerid."'))")
      ->count();

    $output = [
      'draw' => (int) $draw,
      'recordsTotal' => $total,
      'recordsFiltered' => $filtered,
      'data' => $data
    ];

    return json_encode($output);
  }

  public function storeParamsSuspectTrx(Request $req){
    $param = [];
    $start_date = date('Y-m-d', strtotime($req->start_date)). ' 00:00:00';
    $end_date = date('Y-m-d', strtotime($req->end_date)). ' 23:59:59';

    $start_date_e = date('Y-m-d'). ' 00:00:00';
    $end_date_e = date('Y-m-d'). ' 23:59:59';
    if(!empty($req->orderid)) array_push($param, 'tb_r_solusiorder.Orderid LIKE \'%' .$req->orderid. '%\'');
    if(!empty($req->trackingref)) array_push($param, 'tb_r_solusiorder.trackingref LIKE \'%' .$req->trackingref. '%\'');
    if(!empty($req->bookid)) array_push($param, 'tb_r_solusiorder.bookid LIKE \'%' .$req->bookid. '%\'');
    if(!empty($start_date)&& !empty($end_date)) array_push($param , "tb_r_solusiorder.createdon between '" .$start_date. "' and '" .$end_date. "'");
    if(empty($start_date) || empty($end_date)) array_push($param , "tb_r_solusiorder.createdon between '" .$start_date_e. "' and '" .$end_date_e. "'");

    if(count($param) > 0) {
      $where = implode(' and ',$param);
    } else {
      $where = "1";
    }

    return $where;
  }

  function getDataSuspectTrx($where, $start = null, $length = null,$order = null,$type = null,$partnerid){
    
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $where = "tb_r_solusiorder.partnerid = '$this->clientId' and " . $where;
    }else{
      if($partnerid <> "ALL"){
        $where = "tb_r_solusiorder.partnerid = '$partnerid' and " . $where;
      }
    }

    $dataSet = DB::table('tb_r_solusiorder')
                  ->whereRaw('tb_r_solusiorder.isfinish = "2" AND tb_r_solusiorder.statuspay = "SUCCESS"')
                  ->whereRaw($where);

    if(!empty($length))
      $dataSet->take($length);
    if(!empty($length))
      $dataSet->skip($start);
    if(empty($order))
      $dataSet->orderBy('tb_r_solusiorder.finishdate','desc');
    else {

    }

    $data = [];
    $token = null;
    foreach ($dataSet->get() as $d) {
      $data[] = [
        'orderid' =>$d->Orderid,
        'trackingref' => $d->trackingref,
        'bookid' => $d->bookid,
        'amount' =>number_format($d->amount, 0),
        'finishdate' => $d->finishdate
      ];
    }

    return $data;
  }

  public function apiForceSuccess(Request $req){

    $sqlUpdate = DB::table('tb_r_solusiorder')
                    ->where('Orderid',$req->orderid)
                    ->update([
                      'isFinish' => '1',
                      'UpdateOn' => Now()
                    ]);
  }

  public function apiRefund(Request $req){

    $sqlUpdate = DB::table('tb_r_solusiorder')
                    ->where('Orderid',$req->orderid)
                    ->update([
                      'isFinish' => '1',
                      'statuspay' => 'REFUND',
                      'UpdateOn' => Now()
                    ]);
    
    $sqlInsert = DB::insert('
                    INSERT INTO tb_r_solusiwallet_history(
                    SELECT UUID(), a.userid,a.partnerid,a.Amount,NOW(),concat("REFUND MANUAL ORDER ID: ", ?),UUID() 
                    FROM tb_r_solusiorder a
                    WHERE Orderid = ?)'
                    ,[$req->orderid,$req->orderid]);
  }

  // Payment Method
  public function apiDataPM(Request $req){
    $start = $req->start;
    $length = $req->length;
    $draw = $req->draw;
    $order = $req->order;
    $type = '';

    if($req->paymentid <> "ALL"){
      $dataSet = DB::table('tb_m_paymentmethode')->where('pay_methode','=',$req->paymentid);
    }else{
      $dataSet = DB::table('tb_m_paymentmethode');
    }

    if(!empty($length))
      $dataSet->take($length);
    if(!empty($length))
      $dataSet->skip($start);
    if(empty($order))
      $dataSet->orderBy('tb_m_paymentmethode.pay_methode','Asc')
              ->orderBy('tb_m_paymentmethode.methode_desc','Asc');
    else {

    }

    $data = [];
    $token = null;
    foreach ($dataSet->get() as $d) {
      $data[] = [
        'pay_methode' => $d->pay_methode,
        'methode_desc' =>$d->methode_desc,
        'IsAccess' => $d->IsAccess,
      ];
    }

    $total = DB::connection('mysql')->table('tb_m_paymentmethode')->count();

    $filtered = DB::connection('mysql')
      ->table('tb_m_paymentmethode')
      ->whereRaw("((tb_m_paymentmethode.pay_methode = '".$req->paymentid."') OR ('ALL' = '".$req->paymentid."'))")
      ->count();

    $output = [
      'draw' => (int) $draw,
      'recordsTotal' => $total,
      'recordsFiltered' => $filtered,
      'data' => $data
    ];

    return json_encode($output);
  }

  public function apiAddPM(Request $req){
    DB::table('tb_m_paymentmethode')->insert([
      'pay_methode' => $req->name,
      'methode_desc' => $req->desc,
      'IsAccess' => $req->status
    ]);
  }

  public function apiUpdatePM(Request $req){
    if($req->status == '1'){
      $isAccess = '0';
    }else{
      $isAccess = '1';
    }
    DB::table('tb_m_paymentmethode')
      ->where('pay_methode', '=', $req->pay_methode)
      ->update([
        'IsAccess' => $isAccess
      ]);
  }

  public function apiDashboard(Request $req){
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $partnerid = $this->clientId;
    }else{
      $partnerid = $req->partnerid;
    }

    $start = $req->start;
    $length = $req->length;
    $draw = $req->draw;
    $order = $req->order;

    $where = $this->storeParamsDashboard($req);
    $type = '';

    $data = $this->getDataDashboard($where,$start,$length,$order,$type,$req->partnerid);

    $total = DB::connection('mysql')->table('tb_r_solusiorder')
      ->join('tb_m_biller','tb_m_biller.biller_id','=','tb_r_solusiorder.billerid')
      ->selectraw('tb_m_biller.biller_name, COUNT(tb_r_solusiorder.Orderid) as jml_trx')
      ->whereRaw($where)
      ->whereRaw("tb_r_solusiorder.finishdate BETWEEN '. $start_date .' AND ' . $end_date . '")
      ->groupBy('tb_m_biller.biller_name')
      ->count();

    $filtered = DB::connection('mysql')->table('tb_r_solusiorder')
      ->join('tb_m_biller','tb_m_biller.biller_id','=','tb_r_solusiorder.billerid')
      ->selectraw('tb_m_biller.biller_name, COUNT(tb_r_solusiorder.Orderid) as jml_trx')
      ->whereRaw($where)
      ->whereRaw("tb_r_solusiorder.finishdate BETWEEN '. $start_date .' AND ' . $end_date . '")
      ->groupBy('tb_m_biller.biller_name')
      ->count();

    $output = [
      'draw' => (int) $draw,
      'recordsTotal' => $total,
      'recordsFiltered' => $filtered,
      'data' => $data
    ];

    return json_encode($output);

  }
  
  public function storeParamsDashboard(Request $req){
    $param = [];

    if(!empty($req->partnerid)) array_push($param, "tb_r_solusiorder.partnerid =  '" .$req->partnerid. "'" );

    if(count($param) > 0) {
      $where = implode(' and ',$param);
    } else {
      $where = "1";
    }

    return $where;
  }

  function getDataDashboard($where, $start = null, $length = null,$order = null,$type = null,$partnerid){
    if($this->clientId != 'SA' && $this->clientId != 'IDS') {
      $where = "tb_r_solusiorder.partnerid = '$this->clientId' and " . $where;
    }else{
      if($partnerid <> "ALL"){
        $where = "tb_r_solusiorder.partnerid = '$partnerid' and " . $where;
      }
    }

    $start_date = date('Y-m-d'). ' 00:00:00';
    $end_date = date('Y-m-d'). ' 23:59:59';

    $dataSet = DB::table('tb_r_solusiorder')
      ->join('tb_m_biller','tb_m_biller.biller_id','=','tb_r_solusiorder.billerid')
      ->selectraw('tb_m_biller.biller_name, COUNT(tb_r_solusiorder.Orderid) as jml_trx')
      ->whereRaw($where)
      ->whereRaw("tb_r_solusiorder.finishdate BETWEEN '. $start_date .' AND ' . $end_date . '")
      ->groupBy('tb_m_biller.biller_name');

    if(!empty($length))
      $dataSet->take($length);
    if(!empty($length))
      $dataSet->skip($start);
    if(empty($order))
      $dataSet->orderBy('jml_trx','desc');
    else {

    }

    $data = [];
    $token = null;
    foreach ($dataSet->get() as $d) {
      $data[] = [
        'biller_name' =>$d->biller_name,
        'jml_trx' => $d->jml_trx
      ];
    }

    return $data;
  }


}
