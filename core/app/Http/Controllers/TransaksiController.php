<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Route;
use Validator;
use App\Models\UserModel;
use App\Models\ConfigModel;
use Input;
use Hash;
use Crypt;

class TransaksiController extends Controller {

  var $dataUser = false;

  public function __construct()
  {
    $this->dataUser = $this->getDataUser();
    if(empty(Auth::user()->id)) {
        return redirect(route('login'));
    }
    $this->clientId = strtoupper($this->dataUser->role_name);
  }

  public static function getDataUser() {
    $dataUser = DB::table('tb_m_user_solusi')
      ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
      ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
      ->first();
    return $dataUser;
  }

  public static function getDataBiller()
  {
    $dataBiller = DB::table('tb_m_biller')
        ->where('tb_m_biller.is_active', '=', '1')
        ->orderby('biller_name','ASC')
        ->get();
    return $dataBiller;
  }

  public static function getDataProduct($id)
  {
    $dataProduct = DB::table('tb_m_biller_solusi')
        ->where('tb_m_biller_solusi.isactive', '=', '1')
        ->get();
    return $dataProduct;
  }

  public static function getDataPartner()
  {
    $dataPartner = DB::table('tb_m_partner')
        ->where('tb_m_partner.isactive', '=', '1')
        ->get();
    return $dataPartner;
  }

  public function showMain() {
    $data['username'] = $this->dataUser->username;
    $data['data_user'] = $this::getDataUser();
    $data['data_biller'] = $this::getDataBiller();
    $data['data_partner'] = $this::getDataPartner();
    $data['clientID'] = $this->clientId;
    $data['title'] = 'Transaction';
    $data['subtitle'] = 'List Transaction';
    $data['sub_'] = '';
    $data['page'] = 'content.transaksi.index';

    return view('template.content')->with($data);
  }
}