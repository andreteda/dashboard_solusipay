<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Illuminate\Support\Facades\Input;
use Hash;
use Crypt;
use Excel;
use App\Models\ConfigModel;
use Khill\Lavacharts\Lavacharts;
use Mail;

class ChPassController extends Controller
{
    var $dataUser = false;
    public function __construct()
    {
        $this->dataUser = $this->getDataUser();
        if(empty(Auth::user()->id)) {
            return redirect(route('login'));
        }
    }

    public static function getDataUser()
    {
        $dataUser = DB::table('tb_m_user_solusi')
            ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
            ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
            ->first();
        return $dataUser;
    }

    public function showMain() {
        $data['username'] = $this->dataUser->username;
        $data['data_user'] = $this::getDataUser();
        $data['title'] = 'Change Password';
        $data['subtitle'] = 'Change Password';
        $data['sub_'] = '';
        $data['page'] = 'content.chpass.index';
        return view('template.content')->with($data);
    }
}
