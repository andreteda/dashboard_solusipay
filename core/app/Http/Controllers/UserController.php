<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use App\Models\UserModel;
use App\Models\ConfigModel;
use Input;
use Hash;
use Crypt;
use Khill\Lavacharts\Lavacharts;

class UserController extends Controller {
    public static function getDataUser() {
        $dataUser = DB::table('tb_m_user_solusi')
                ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
                ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
                ->first();
        return $dataUser;
    }

    public function showUserLevel() {
        if(empty(Auth::user()->id)) {
            return redirect(route('login'));
        } else {
            $data['data_user'] = $this::getDataUser();
            $data['title'] = 'User Level';
            $data['subtitle'] = 'User Solusipay Web';
            $data['sub_'] = 'Level';
            $data['page'] = 'content.user-level.index';
            return view('template.content')->with($data);
        }
    }

    public function showUserList() {
        if(empty(Auth::user()->id)) {
            return redirect(route('login'));
        } else {
            $data['data_user'] = $this::getDataUser();
            $data['title'] = 'User List';
            $data['subtitle'] = 'User Solusipay Web';
            $data['sub_'] = 'List';
            $data['page'] = 'content.user-level.list-user';
            $data['role'] = DB::table('tb_m_user_role')->get();
            return view('template.content')->with($data);
        }
    }
}
