<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use App\Models\UserModel;
use App\Models\ConfigModel;
use Input;
use Hash;
use Crypt;
use Khill\Lavacharts\Lavacharts;

class PaymentMethodController extends Controller
{
    public static function getDataUser() {
        $dataUser = DB::table('tb_m_user_solusi')
                ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
                ->where('tb_m_user_solusi.id', '=', Auth::user()->id)
                ->first();
        return $dataUser;
    }

    public static function getDataPartner(){
        $dataPartner = DB::table('tb_m_partner')
            ->where('tb_m_partner.isactive', '=', '1')
            ->get();
        return $dataPartner;
    }

  public function showMain() {
      if(empty(Auth::user()->id)) {
          return redirect(route('login'));
      } else {
          $data['data_user'] = $this::getDataUser();
          $data['data_pm'] = DB::table('tb_m_paymentmethode')->get();
          $data['data_partner'] = $this::getDataPartner();
          $data['title'] = 'Payment Method';
          $data['subtitle'] = 'Payment Method';
          $data['sub_'] = '';
          $data['page'] = 'content.payment-method.index';
          return view('template.content')->with($data);
      }
  }
}
