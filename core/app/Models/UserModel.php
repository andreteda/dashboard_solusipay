<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\AuditingTrait;
class UserModel extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract{
									
	use Authenticatable, Authorizable, CanResetPassword;
	use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'tb_m_user_solusi';	
	protected $primaryKey  = 'id';
	protected $hidden = ['password'];
	public function getAuthPassword()
    {
        return $this->password;
    }
		
}
