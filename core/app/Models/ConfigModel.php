<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ConfigModel extends Model {

    public static function getTotalAmount(){
        $total = DB::table('tb_m_rekonsiliasi')->select(DB::raw('SUM(amount) as total'))->first();
        return $total->total;
    }

    public static function getTotalAmountMandiri(){
        $total = DB::table('tb_m_rekonsiliasi')->select(DB::raw('SUM(amount) as total'))->where('pay_method', '=', 'Bank Mandiri (Virtual Account)')->first();
        return $total->total;
    }

    public static function getTotalAmountBCA(){
        $total = DB::table('tb_m_rekonsiliasi')->select(DB::raw('SUM(amount) as total'))->where('pay_method', '=', 'BCA (Virtual Account)')->first();
        return $total->total;
    }

    public static function getTotalAmountBRI(){
        $total = DB::table('tb_m_rekonsiliasi')->select(DB::raw('SUM(amount) as total'))->where('pay_method', '=', 'Bank BRI (Virtual Account)')->first();
        return $total->total;
    }

    public static function getListRole(){
        $total = DB::table('tb_m_user_role')->get();
        return $total;
    }

    public static function listTotalAmount(){
        $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->groupby('pay_method')
                ->get();
        return $total;
    }

    public static function getDataUser() {
        $dataUser = DB::table('tb_m_user_solusi')
                ->join('tb_m_user_role', 'tb_m_user_solusi.roleid', '=', 'tb_m_user_role.role_id')
                ->get();
        return $dataUser;
    }

    public static function getLastConfig($config_name){
        $config = ConfigModel::where('config_name', '=', $config_name)
            ->select('*')
            ->first();
        return $config->config_value;
    }

    public static function loadPaymentOverview($daterange) {
        if($daterange == '01/01/2018 - 01/15/2018') {
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("DATE_FORMAT(trx_date, '%M') AS y"), DB::raw("SUM(CASE WHEN pay_method = 'BCA (Virtual Account)' THEN amount ELSE 0 END) AS a"), DB::raw("SUM(CASE WHEN pay_method = 'Bank BRI (Virtual Account)' THEN amount ELSE 0 END) AS b"), DB::raw("SUM(CASE WHEN pay_method = 'Bank Mandiri (Virtual Account)' THEN amount ELSE 0 END) AS c"))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("DATE_FORMAT(trx_date, '%M') AS y"), DB::raw("SUM(CASE WHEN pay_method = 'BCA (Virtual Account)' THEN amount ELSE 0 END) AS a"), DB::raw("SUM(CASE WHEN pay_method = 'Bank BRI (Virtual Account)' THEN amount ELSE 0 END) AS b"), DB::raw("SUM(CASE WHEN pay_method = 'Bank Mandiri (Virtual Account)' THEN amount ELSE 0 END) AS c"))
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        }
        return $db;
    }

    public static function labelStatus($daterange) {
        if($daterange == '01/01/2018 - 01/15/2018') {
            $db = DB::table('tb_m_rekonsiliasi') 
                ->select(DB::raw("DATE_FORMAT(trx_date, '%M') AS y"))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("DATE_FORMAT(trx_date, '%M') AS y"))
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        }
        $data = [];
        foreach ($db as $d) {
            $data[] = [
                $d->y
            ];
        }
        return $data;
        //return '"' .str_replace(',', '", "', rtrim($bulan,",")). '"';
    }

    public static function suksesStatus($daterange) {
        if($daterange == '01/01/2018 - 01/15/2018') {
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("SUM(CASE WHEN `status` = 'SUCCESS' THEN 1 ELSE 0 END) AS a"))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("SUM(CASE WHEN `status` = 'SUCCESS' THEN 1 ELSE 0 END) AS a"))
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        }
        $data = [];
        foreach ($db as $d) {
            $data[] = 
                $d->a
            ;
        }
        $datax = "1,2,3,4";
        return $data;
    }

    public static function pendingStatus($daterange) {
        if($daterange == '01/01/2018 - 01/15/2018') {
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("SUM(CASE WHEN `status` = 'PENDING' THEN 1 ELSE 0 END) AS b"))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("SUM(CASE WHEN `status` = 'PENDING' THEN 1 ELSE 0 END) AS b"))
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        }
        $data = [];
        foreach ($db as $d) {
            $data[] = 
                $d->b
            ;
        }
        return $data;
    }

    public static function totalStatus($daterange) {
        if($daterange == '01/01/2018 - 01/15/2018') {
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("(SUM(CASE WHEN `status` = 'SUCCESS' THEN 1 ELSE 0 END)+SUM(CASE WHEN `status` = 'PENDING' THEN 1 ELSE 0 END)) AS total"))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $db = DB::table('tb_m_rekonsiliasi')
                ->select(DB::raw("(SUM(CASE WHEN `status` = 'SUCCESS' THEN 1 ELSE 0 END)+SUM(CASE WHEN `status` = 'PENDING' THEN 1 ELSE 0 END)) AS total"))
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->groupby(DB::raw("DATE_FORMAT(trx_date, '%M')"))
                ->orderby(DB::raw("MONTH(trx_date)"))
                ->get();
        }
        $data = [];
        foreach ($db as $d) {
            $data[] = 
                $d->total
            ;
        }
        //return json_encode(rtrim($datax,","));
        return $data;
    }

    public static function totalTransaksi($daterange){
        if($daterange == '01/01/2018 - 01/15/2018') {
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->groupby('pay_method')
                ->get();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d H:i:s', strtotime($date_range[1]));
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->groupby('pay_method')
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->get();
        }
        return $total;
    }

    public static function listData($id) {
        $data = DB::table('tb_m_rekonsiliasi')
                ->where('pay_method', 'LIKE', '%'.$id.'%')
                ->get();
        return $data;
    }

    public static function transactionDANA(){
        $data = DB::connection('mysql2')->table('tb_r_orderdatav2')
                ->select('tracking_ref', 'billerdesc', 'productdesc', 'createdtime', 'userid', 'customername', 'amount', '`status`')
                ->leftjoin('tb_m_biller_product', 'tb_m_biller_product.billerid', '=', 'tb_r_orderdatav2.billerid')
                ->whereNotNull('customername')
                ->where('tb_r_orderdatav2.issync', '=', '1')
                ->whereRaw('createdtime = NOW()')
                ->get();
        return $data;
    }

    public static function DetailtransactionDANA($id){
        $data = DB::connection('mysql3')->table('dana_tb_r_orderdatav2')
                ->select('tracking_ref', 'billerdesc', 'productdesc', 'dana_tb_m_biller_product.productid', 'createdtime',
                    'userid', 'customername', 'amount', 'rcdesc', 'acquirementid',
                    'customerid', 'receiptcode', 'dana_tb_r_orderdatav2.additionaldata_pay', 'dana_tb_r_orderdatav2.issync',
                    DB::raw('(CASE 
                                    WHEN dana_tb_r_orderdatav2.issync = 0 THEN \'PENDING\' 
                                    WHEN dana_tb_r_orderdatav2.issync = 1 THEN \'SUCCESS\' 
                                    WHEN dana_tb_r_orderdatav2.issync = 9 THEN \'FAILED\' 
                                    ELSE \'SUSPECT\' END) AS status')
                )
                ->leftJoin('dana_tb_m_biller_product', function($join)
                {
                    $join->on('dana_tb_m_biller_product.productid', '=', 'dana_tb_r_orderdatav2.productid');
                    $join->on('dana_tb_m_biller_product.billerid', '=', 'dana_tb_r_orderdatav2.billerid');
                })
                ->where('tracking_ref', '=', $id)
                ->first();
        return $data;
    }

    public static function DetailtransactionTodayDANA($id){
        $data = DB::connection('mysql2')->table('tb_r_orderdatav2')
            ->select('tb_r_orderdatav2.tracking_ref', 'billerdesc', 'productdesc', 'createdtime', 'userid', 'customername',
                'amount', 'tb_r_orderdatav2.status', 'rcdesc', 'acquirementid', 'customerid', 'receiptcode',
                'tb_r_orderdatav2.additionaldata_pay', 'receiptcode', 'tb_r_orderdatav2.issync',
                DB::raw('(CASE 
                                WHEN tb_r_orderdatav2.issync = 0 THEN \'PENDING\' 
                                WHEN tb_r_orderdatav2.issync = 1 THEN \'SUCCESS\' 
                                WHEN tb_r_orderdatav2.issync = 9 THEN \'FAILED\' 
                                ELSE \'SUSPECT\' END) AS status')
            )
            ->leftJoin('tb_m_biller_product', function($join)
            {
                $join->on('tb_m_biller_product.productid', '=', 'tb_r_orderdatav2.productid');
                $join->on('tb_m_biller_product.billerid', '=', 'tb_r_orderdatav2.billerid');
            })
            ->where('tracking_ref', '=', $id)
            ->first();
        return $data;
    }

    public static function getToken($id) {
        $data = DB::connection('mysql2')->table('tb_r_orderdatav2')
                ->leftjoin('tb_m_biller_product', 'tb_m_biller_product.billerid', '=', 'tb_r_orderdatav2.billerid')
                ->where('tracking_ref', '=', $id)
                ->first();
        $data_exp = explode("ô", $data->additionaldata_pay);
        return $data_exp[25];
    }
    
    public static function totalTransaksiMandiri($daterange){
        if($daterange == '01/01/2018 - 01/15/2018') {
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                //->groupby('pay_method')
                ->where('pay_method', '=', 'Bank Mandiri (Virtual Account)')
                ->first();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->where('pay_method', '=', 'Bank Mandiri (Virtual Account)')
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->first();
        }
        return $total;
    }

    public static function totalTransaksiBRI($daterange){
        if($daterange == '01/01/2018 - 01/15/2018') {
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                //->groupby('pay_method')
                ->where('pay_method', '=', 'Bank BRI (Virtual Account)')
                ->first();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->where('pay_method', '=', 'Bank BRI (Virtual Account)')
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->first();
        }
        return $total;
    }

    public static function totalTransaksiBCA($daterange){
        if($daterange == '01/01/2018 - 01/15/2018') {
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                //->groupby('pay_method')
                ->where('pay_method', '=', 'BCA (Virtual Account)')
                ->first();
        } else {
            $date_range = explode(" - ", $daterange);
            $start_date = date('Y-m-d H:i:s', strtotime($date_range[0]));
            $end_date = date('Y-m-d', strtotime($date_range[1])). ' 23:59:59';
            $total = DB::table('tb_m_rekonsiliasi')
                ->select("pay_method", DB::raw('SUM(amount) as total'))
                ->where('pay_method', '=', 'BCA (Virtual Account)')
                ->whereBetween('trx_date', array($start_date, $end_date))
                ->first();
        }
        return $total;
    }

    public static function banner() {
        $db = DB::table('banners')->get();
        return $db;
    }

    public static function dataBanner($id) {
        $db = DB::table('banners')->where('id', '=', $id)->first();
        return $db;
    }

    public static function getDataRepeated() {
        $db = DB::table('tb_r_schedule_repeated')->leftjoin('tb_m_days', 'tb_m_days.id', '=', 'tb_r_schedule_repeated.day')->select('tb_r_schedule_repeated.id', 'day_name', 'time', 'recipient')->get();
        return $db;
    }

    public static function getDataRecipient() {
        $db = DB::table('tb_m_recipient')->get();
        return $db;
    }

    public static function dataCron($id) {
        $db = DB::table('tb_r_schedule_repeated')->where('id', '=', $id)->first();
        return $db;
    }

    public static function dataRecipient($id) {
        $db = DB::table('tb_m_recipient')->where('id', '=', $id)->first();
        return $db;
    }

    public static function dataExport($date) {
        $db = DB::table('tb_r_solusiorder as a')
                ->select('a.Orderid', 'b.Keterangan', 'a.MobilePhone', 'a.Customerid', 'a.customername', 'a.Amount', 'pay_methode_desc', 'a.no_transaction', 'promo_amount', 'kode_promo', 'a.CreatedOn', 'a.paydate', 'a.finishdate', 'a.statuspay', 'a.kode_promo', 'a.promo_amount')
                ->leftJoin('tb_m_biller_solusi as b', function($join) {
                    $join->on('a.billerid', '=', 'b.Billerid')
                    ->on('a.productid', '=', 'b.Productid');
                })
                ->where('statuspay', '=', 'success')
                ->where(DB::raw('date_format(CreatedOn,"%Y%m%d")'), '=', $date)->get();
        $datax=[];
        foreach ($db as $d) {
            $datax[] = [
                'Orderid' =>$d->Orderid,
                'Keterangan' => $d->Keterangan,
                'MobilePhone' => $d->MobilePhone,
                'Customerid' => $d->Customerid,
                'customername' => $d->customername,
                'Amount' => $d->Amount,
                'pay_methode_desc' => $d->pay_methode_desc,
                'no_transaction' => $d->no_transaction,
                'promo_amount' => $d->promo_amount,
                'kode_promo' => $d->kode_promo,
                'CreatedOn' => $d->CreatedOn,
                'paydate' => $d->paydate,
                'finishdate' => $d->finishdate,
                'statuspay' => $d->statuspay

            ];
        }
        return $datax;
    }
}
