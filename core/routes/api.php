<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

// Setting Cors Origin
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

/*Route::get('promo', 'PromoController@listPromo');
Route::post('promoCode', 'PromoController@promoCode');
Route::post('list-promo', 'PromoController@listPromo2');
Route::post('productpromo', 'ProductBillerController@PromoProduct');

Route::get('list-banners', 'BannerController@listBanner');
Route::get('list-biller', 'ProductBillerController@listBiller2');
Route::post('list-product', 'ProductBillerController@listProduct');
Route::post('list-product2', 'ProductBillerController@listProduct2');
Route::post('Getreff', 'ProductBillerController@cek_reff');
Route::post('list-report', 'ReportController@listReportJSON');
*/
