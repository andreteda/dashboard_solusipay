<?php

// Auth
Route::get('/', [
  'middleware' => 'auth', 'as' => 'dashboard', 'uses' => 'CoreController@showDashboard'
]);

Route::get('auth/login', [
  'as' => 'login', 'uses' => 'CoreController@showLoginv2'
]);

Route::get('auth/logout', [
  'middleware' => 'auth', 'as' => 'do-logout', 'uses' => 'CoreController@doLogout'
]);

Route::post('login/do', [
  'as' => 'do-login', 'uses' => 'CoreController@doLogin'
]);

Route::get('check', [
  'as' => 'check', 'uses' => 'CoreController@check'
]);

Route::group(['prefix' => 'user'], function () {
  Route::get('user-level', [
    'as' => 'user-level', 'uses' => 'UserController@showUserLevel', 'middleware' => 'auth:sa'
  ]);
  Route::get('user-list', [
    'as' => 'user-list', 'uses' => 'UserController@showUserList', 'middleware' => 'auth:sa'
  ]);
  Route::post('detail-user', [
    'as' => 'detail-user', 'uses' => 'APIController@apiDetailUser', 'middleware' => 'auth:sa'
  ]);
});

// chpass
Route::get('chpass', [
  'as' => 'chpass', 'uses' => 'ChPassController@showMain', 'middleware' => 'auth:sa'
]);

// Product
Route::get('product', [
  'as' => 'product', 'uses' => 'ProductController@showMain', 'middleware' => 'auth:sa'
]);


// rekon-transaksi
Route::get('rekon-transaksi', [
  'as' => 'rekon-transaksi', 'uses' => 'RekonTransaksiController@showMain', 'middleware' => 'auth:sa-sps-ays-dji-mfs'
]);

// Transaksi
Route::get('transaksi', [
  'as' => 'transaksi', 'uses' => 'TransaksiController@showMain', 'middleware' => 'auth:sa-sps-ays-dji'
]);

// Suspect Transaksi
Route::get('suspect-transaksi', [
  'as' => 'suspect-transaksi', 'uses' => 'SuspectTransaksiController@showMain', 'middleware' => 'auth:sa-sps-ays-dji'
]);

// Payment Method
Route::get('payment-method', [
  'as' => 'payment-method', 'uses' => 'PaymentMethodController@showMain', 'middleware' => 'auth:sa-sps-ays-dji'
]);

// API
Route::group(['prefix' => 'api'], function () {

  // Users
  Route::get('data-role', [
    'as' => 'data-role', 'uses' => 'APIController@apiDataRole', 'middleware' => 'auth:sa'
  ]);
  Route::post('add-role', [
    'as' => 'add-role', 'uses' => 'APIController@apiAddRole', 'middleware' => 'auth:sa'
  ]);
  Route::post('delete-role', [
    'as' => 'delete-role', 'uses' => 'APIController@apiDeleteRole', 'middleware' => 'auth:sa'
  ]);
  Route::get('data-user', [
    'as' => 'data-user', 'uses' => 'APIController@apiDataUser', 'middleware' => 'auth:sa'
  ]);
  Route::post('add-user', [
    'as' => 'add-user', 'uses' => 'APIController@apiAddUser', 'middleware' => 'auth:sa'
  ]);
  Route::post('delete-user', [
    'as' => 'delete-user', 'uses' => 'APIController@apiDeleteUser', 'middleware' => 'auth:sa'
  ]);

  // Dashboard
  Route::post('load-dashboard', [
    'as' => 'load-dashboard', 'uses' => 'APIController@apiDashboard', 'middleware' => 'auth:sa'
  ]);

  // CHPass
  Route::post('update-chpass', [
    'as' => 'update-chpass', 'uses' => 'APIController@chPass', 'middleware' => 'auth:sa'
  ]);

  // Product
  Route::post('list-product', [
    'as' => 'list-product', 'uses' => 'APIController@apiProduct', 'middleware' => 'auth:sa'
  ]);
  Route::post('data-product', [
    'as' => 'data-product', 'uses' => 'APIController@apiDataProduct', 'middleware' => 'auth:sa'
  ]);
  Route::post('add-product', [
    'as' => 'add-product', 'uses' => 'APIController@apiAddProduct', 'middleware' => 'auth:sa'
  ]);
  Route::post('delete-product', [
    'as' => 'delete-product', 'uses' => 'APIController@apiDeleteProduct', 'middleware' => 'auth:sa'
  ]);
  Route::post('detail-product', [
    'as' => 'detail-product', 'uses' => 'APIController@apiDetailProduct', 'middleware' => 'auth:sa'
  ]);

  // Transaksi
  Route::post('load-transaksi', [
    'as' => 'load-transaksi', 'uses' => 'APIController@apiTransaksi', 'middleware' => 'auth:sa'
  ]);
  Route::get('export-transaksi', [
    'as' => 'export-transaksi', 'uses' => 'APIController@apiExportTransaksi', 'middleware' => 'auth:sa'
  ]);

  // Suspect Transaksi
  Route::post('load-suspecttrx', [
    'as' => 'load-suspecttrx', 'uses' => 'APIController@apiSuspectTransaksi', 'middleware' => 'auth:sa'
  ]);
  Route::post('force-success', [
    'as' => 'force-success', 'uses' => 'APIController@apiForceSuccess', 'middleware' => 'auth:sa'
  ]);
  Route::post('refund', [
    'as' => 'refund', 'uses' => 'APIController@apiRefund', 'middleware' => 'auth:sa'
  ]);

  // Payment Method
  Route::post('data-pm', [
    'as' => 'data-pm', 'uses' => 'APIController@apiDataPM', 'middleware' => 'auth:sa'
  ]);
  Route::post('add-pm', [
    'as' => 'add-pm', 'uses' => 'APIController@apiAddPM', 'middleware' => 'auth:sa'
  ]);
  Route::post('update-pm', [
    'as' => 'update-pm', 'uses' => 'APIController@apiUpdatePM', 'middleware' => 'auth:sa'
  ]);

  //Rekon Transaksi
  Route::post('load-rekon-transaksi', [
    'as' => 'load-rekon-transaksi', 'uses' => 'APIController@apiRekonTransaksi', 'middleware' => 'auth:sa'
  ]);
  Route::get('export-rekon-transaksi', [
    'as' => 'export-rekon-transaksi', 'uses' => 'APIController@apiExportRekonTransaksi', 'middleware' => 'auth:sa'
  ]);
});
